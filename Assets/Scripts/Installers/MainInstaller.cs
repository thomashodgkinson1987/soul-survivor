using System.Collections.Generic;
using Muffin;
using UnityEngine;
using Zenject;

public class MainInstaller : MonoInstaller<MainInstaller>
{

    #region Fields

    [SerializeField]
    private AudioSource m_musicAudioSource;
    [SerializeField]
    private AudioSource m_soundAudioSource;

    [Space]

    [SerializeField]
    private TextAsset m_brainDataJson;
    [SerializeField]
    private TextAsset m_actorControllerDataJson;
    [SerializeField]
    private TextAsset m_physicsControllerDataJson;

    [Space]

    [SerializeField]
    private List<AnimatorOverrideController> m_animatorControllers;

    #endregion // Fields



    #region Installer overrides

    public override void InstallBindings ()
    {
        BindAudioManager(Container);

        BindFactories(Container);

        BindAnimatorControllerRepository(Container);
        BindPhysicsControllerDataRepository(Container);
        BindBrainDataRepository(Container);
        BindActorControllerDataRepository(Container);

        BindRepositoryLoader(Container);

        BindGameWorld(Container);
    }

    #endregion // Installer overrides



    #region Private methods

    private void BindAudioManager (DiContainer container)
    {
        container.Bind<IAudioManager>()
            .To<AudioManager>()
            .AsSingle()
            .WithArguments(m_musicAudioSource, m_soundAudioSource);
    }

    private void BindFactories (DiContainer container)
    {
        container.Bind<IInputControllerFactory>()
            .To<InputControllerFactory>()
            .AsSingle();

        container.Bind<IInputResolverFactory>()
            .To<InputResolverFactory>()
            .AsSingle();

        container.Bind<IPhysicsControllerFactory>()
            .To<PhysicsControllerFactory>()
            .AsSingle();

        container.Bind<IPhysicsResolverFactory>()
            .To<PhysicsResolverFactory>()
            .AsSingle();

        container.Bind<IBrainDataFactory>()
            .To<BrainDataFactory>()
            .AsSingle();

        container.Bind<IActorControllerDataFactory>()
            .To<ActorControllerDataFactory>()
            .AsSingle();

        container.Bind<IPhysicsControllerDataFactory>()
            .To<PhysicsControllerDataFactory>()
            .AsSingle();

        container.Bind<IBrainFactory>()
            .To<BrainFactory>()
            .AsSingle();

        container.Bind<IActorControllerFactory>()
            .To<ActorControllerFactory>()
            .AsSingle();
    }

    private void BindAnimatorControllerRepository (DiContainer container)
    {
        container.Bind<IRepositoryDataGetter<AnimatorOverrideController>>()
            .To<AnimatorControllerRepositoryDataGetter>()
            .AsSingle()
            .WithArguments(m_animatorControllers);

        container.Bind<IRepository<AnimatorOverrideController>>()
            .To<AnimatorControllerRepository>()
            .AsSingle();
    }
    private void BindPhysicsControllerDataRepository (DiContainer container)
    {
        container.Bind<IRepositoryDataGetter<PhysicsControllerData>>()
            .To<JsonRepositoryDataGetter<PhysicsControllerData>>()
            .AsSingle()
            .WithArguments(m_physicsControllerDataJson.text);

        container.Bind<IRepository<PhysicsControllerData>>()
            .To<PhysicsControllerDataRepository>()
            .AsSingle();
    }
    private void BindBrainDataRepository (DiContainer container)
    {
        container.Bind<IRepositoryDataGetter<BrainData>>()
            .To<JsonRepositoryDataGetter<BrainData>>()
            .AsSingle()
            .WithArguments(m_brainDataJson.text);

        container.Bind<IRepository<BrainData>>()
            .To<BrainDataRepository>()
            .AsSingle();
    }
    private void BindActorControllerDataRepository (DiContainer container)
    {
        container.Bind<IRepositoryDataGetter<ActorControllerData>>()
            .To<JsonRepositoryDataGetter<ActorControllerData>>()
            .AsSingle()
            .WithArguments(m_actorControllerDataJson.text);

        container.Bind<IRepository<ActorControllerData>>()
            .To<ActorControllerDataRepository>()
            .AsSingle();
    }

    private void BindRepositoryLoader (DiContainer container)
    {
        container.Bind<IRepositoryLoader>()
            .To<RepositoryLoader>()
            .AsSingle();
    }

    private void BindGameWorld (DiContainer container)
    {
        container.Bind<GameWorld>()
            .To<GameWorld>()
            .AsSingle()
            .NonLazy();
    }

    #endregion // Private methods

}
