﻿namespace Muffin
{
    public class DefaultActorState : ActorState
    {

        public override void OnEnter (ActorController actor) { }
        public override void OnExit (ActorController actor) { }

        public override void Tick (ActorController actor, float dt) { }
        public override void FixedTick (ActorController actor, float dt) { }
        public override void LateTick (ActorController actor, float dt) { }

    }
}