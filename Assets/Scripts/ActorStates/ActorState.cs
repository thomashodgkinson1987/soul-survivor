﻿namespace Muffin
{
    public abstract class ActorState : IActorState
    {
        public abstract void OnEnter (ActorController actor);
        public abstract void OnExit (ActorController actor);

        public abstract void Tick (ActorController actor, float dt);
        public abstract void FixedTick (ActorController actor, float dt);
        public abstract void LateTick (ActorController actor, float dt);

    }
}
