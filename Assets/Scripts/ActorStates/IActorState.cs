﻿namespace Muffin
{
    public interface IActorState
    {
        void OnEnter (ActorController actor);
        void OnExit (ActorController actor);

        void Tick (ActorController actor, float dt);
        void FixedTick (ActorController actor, float dt);
        void LateTick (ActorController actor, float dt);
    }
}