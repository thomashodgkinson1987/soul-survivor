﻿namespace Muffin
{
    public class ActorStateFactory : IActorStateFactory
    {

        #region Constructors

        public ActorStateFactory () { }

        #endregion // Constructors



        #region Public methods

        public IActorState CreateState (int id = 0)
        {
            switch (id)
            {
                case 0:
                    return new DefaultActorState();

                default:
                    return new DefaultActorState();
            }
        }

        #endregion // Public methods

    }
}
