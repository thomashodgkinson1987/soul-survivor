﻿namespace Muffin
{
    public interface IActorStateFactory
    {
        IActorState CreateState (int id = 0);
    }
}