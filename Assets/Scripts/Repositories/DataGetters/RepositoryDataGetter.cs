﻿using System.Collections.Generic;

namespace Muffin
{
    public abstract class RepositoryDataGetter<T> : IRepositoryDataGetter<T> where T : class
    {

        #region Constructors

        public RepositoryDataGetter () { }

        #endregion // Constructors



        #region Public methods

        public abstract List<T> GetData ();

        #endregion // Public methods

    }
}
