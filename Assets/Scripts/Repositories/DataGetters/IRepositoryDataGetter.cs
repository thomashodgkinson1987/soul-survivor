﻿using System.Collections.Generic;

namespace Muffin
{
    public interface IRepositoryDataGetter<T> where T : class
    {
        List<T> GetData ();
    }
}