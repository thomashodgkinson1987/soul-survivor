﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Muffin
{
    public class JsonRepositoryDataGetter<T> : RepositoryDataGetter<T> where T : class
    {

        #region Fields

        private readonly string m_json;

        #endregion // Fields



        #region Constructors

        public JsonRepositoryDataGetter (string json)
        {
            m_json = json;
        }

        #endregion // Constructors



        #region Public methods

        public override List<T> GetData ()
        {
            return JsonConvert.DeserializeObject<List<T>>(m_json);
        }

        #endregion // Public methods

    }
}
