﻿using System.Collections.Generic;
using UnityEngine;

namespace Muffin
{
    public class AnimatorControllerRepositoryDataGetter : RepositoryDataGetter<AnimatorOverrideController>
    {

        #region Fields

        private readonly List<AnimatorOverrideController> m_controllers;

        #endregion // Fields



        #region Constructors

        public AnimatorControllerRepositoryDataGetter (List<AnimatorOverrideController> controllers)
        {
            m_controllers = new List<AnimatorOverrideController>();

            if (controllers != null)
            {
                for (int i = 0; i < controllers.Count; i++)
                {
                    if (controllers[i] != null)
                    {
                        m_controllers.Add(controllers[i]);
                    }
                }
            }
        }

        #endregion // Constructors



        #region Public methods

        public override List<AnimatorOverrideController> GetData ()
        {
            return m_controllers;
        }

        #endregion // Public methods

    }
}
