﻿using System.Collections.Generic;

namespace Muffin
{
    public interface IRepository<T> where T : class
    {
        int Count { get; }

        void AddEntry (T entry);
        void AddEntries (List<T> entries);
        T GetEntry (int id = 0);
    }
}