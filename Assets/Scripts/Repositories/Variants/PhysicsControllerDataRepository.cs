﻿namespace Muffin
{
    public class PhysicsControllerDataRepository : Repository<PhysicsControllerData>
    {

        #region Fields

        private readonly IPhysicsControllerDataFactory m_physicsControllerDataFactory;

        #endregion // Fields



        #region Constructors

        public PhysicsControllerDataRepository (
            IPhysicsControllerDataFactory physicsControllerDataFactory,
            IRepositoryDataGetter<PhysicsControllerData> dataGetter)
            : base(dataGetter)
        {
            m_physicsControllerDataFactory = physicsControllerDataFactory;
        }

        #endregion // Constructors



        #region Public methods

        protected override PhysicsControllerData RetreiveEntry (int id = 0)
        {
            return m_physicsControllerDataFactory.CreateData(
                m_entries[id].MaxVelocityX,
                m_entries[id].MaxVelocityY,
                m_entries[id].GravityModifierX,
                m_entries[id].GravityModifierY,
                m_entries[id].Acceleration,
                m_entries[id].Deceleration,
                m_entries[id].JumpLimit,
                m_entries[id].JumpHeight,
                m_entries[id].IsApplyGravity,
                m_entries[id].IsClampVelocity,
                m_entries[id].IsBypassJumpLimit,
                m_entries[id].Margin,
                m_entries[id].HorizontalRayCount,
                m_entries[id].VerticalRayCount,
                m_entries[id].LeftLayerMask,
                m_entries[id].RightLayerMask,
                m_entries[id].DownLayerMask,
                m_entries[id].UpLayerMask);
        }

        #endregion // Public methods

    }
}
