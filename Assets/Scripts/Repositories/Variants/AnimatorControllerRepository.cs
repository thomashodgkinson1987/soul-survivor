﻿using UnityEngine;

namespace Muffin
{
    public class AnimatorControllerRepository : Repository<AnimatorOverrideController>
    {

        #region Constructors

        public AnimatorControllerRepository (IRepositoryDataGetter<AnimatorOverrideController> dataGetter)
            : base(dataGetter) { }

        #endregion // Constructors



        #region Public methods

        protected override AnimatorOverrideController RetreiveEntry (int id = 0)
        {
            return m_entries[id];
        }

        #endregion // Public methods

    }
}
