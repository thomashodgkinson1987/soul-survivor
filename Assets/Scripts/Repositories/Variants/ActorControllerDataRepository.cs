﻿namespace Muffin
{
    public class ActorControllerDataRepository : Repository<ActorControllerData>
    {

        #region Fields

        private readonly IActorControllerDataFactory m_actorControllerDataFactory;

        #endregion // Fields



        #region Constructors

        public ActorControllerDataRepository (
            IActorControllerDataFactory actorControllerDataFactory,
            IRepositoryDataGetter<ActorControllerData> dataGetter)
            : base(dataGetter)
        {
            m_actorControllerDataFactory = actorControllerDataFactory;
        }

        #endregion // Constructors



        #region Public methods

        protected override ActorControllerData RetreiveEntry (int id = 0)
        {
            return m_actorControllerDataFactory.CreateData(
                m_entries[id].Name,
                m_entries[id].PluralName,
                m_entries[id].PositionX,
                m_entries[id].PositionY,
                m_entries[id].Width,
                m_entries[id].Height,
                m_entries[id].InputControllerId,
                m_entries[id].InputResolverId,
                m_entries[id].PhysicsControllerId,
                m_entries[id].PhysicsResolverId,
                m_entries[id].PhysicsControllerDataId,
                m_entries[id].AnimatorControllerId);
        }

        #endregion // Public methods

    }
}
