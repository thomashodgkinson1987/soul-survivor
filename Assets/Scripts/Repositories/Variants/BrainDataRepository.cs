﻿namespace Muffin
{
    public class BrainDataRepository : Repository<BrainData>
    {

        #region Fields

        private readonly IBrainDataFactory m_brainDataFactory;

        #endregion // Fields



        #region Constructors

        public BrainDataRepository (
            IBrainDataFactory brainDataFactory,
            IRepositoryDataGetter<BrainData> dataGetter)
            : base(dataGetter)
        {
            m_brainDataFactory = brainDataFactory;
        }

        #endregion // Constructors



        #region Public methods

        protected override BrainData RetreiveEntry (int id = 0)
        {
            return m_brainDataFactory.CreateData(
                m_entries[id].InputControllerId,
                m_entries[id].InputResolverId,
                m_entries[id].PhysicsControllerId,
                m_entries[id].PhysicsResolverId);
        }

        #endregion // Public methods

    }
}
