﻿using System.Collections.Generic;

namespace Muffin
{
    public abstract class Repository<T> : IRepository<T> where T : class
    {

        #region Fields

        protected readonly List<T> m_entries;

        #endregion // Fields



        #region Properties

        public int Count
        {
            get { return m_entries.Count; }
        }

        #endregion // Properties



        #region Constructors

        public Repository (IRepositoryDataGetter<T> dataGetter)
        {
            m_entries = new List<T>();

            if (dataGetter != null)
            {
                List<T> entries = dataGetter.GetData();

                if (entries != null)
                {
                    for (int i = 0; i < entries.Count; i++)
                    {
                        if (entries[i] != null)
                        {
                            m_entries.Add(entries[i]);
                        }
                    }
                }
            }
        }

        #endregion // Constructors



        #region Public methods

        public void AddEntry (T entry)
        {
            if (entry != null && !m_entries.Contains(entry))
            {
                m_entries.Add(entry);
            }
        }
        public void AddEntries (List<T> entries)
        {
            if (entries != null)
            {
                for (int i = 0; i < entries.Count; i++)
                {
                    if (entries[i] != null)
                    {
                        AddEntry(entries[i]);
                    }
                }
            }
        }
        public T GetEntry (int id = 0)
        {
            if (id >= 0 && id < Count && m_entries[id] != null)
            {
                return RetreiveEntry(id);
            }

            return null;
        }

        #endregion // Public methods



        #region Abstract methods

        protected abstract T RetreiveEntry (int id = 0);

        #endregion // Abstract methods

    }
}
