﻿namespace Muffin
{
    public class InputResolverFactory : IInputResolverFactory
    {

        #region Constructors

        public InputResolverFactory () { }

        #endregion // Constructors



        #region Public methods

        public IInputResolver CreateInputResolver (int id = 0)
        {
            switch (id)
            {
                case 0:
                    return new InputResolver();

                default:
                    return new InputResolver();
            }
        }

        #endregion // Public methods

    }
}
