﻿namespace Muffin
{
    public interface IInputResolverFactory
    {
        IInputResolver CreateInputResolver (int id = 0);
    }
}