﻿namespace Muffin
{
    public interface IInputResolver
    {
        void OnJumpDown (ActorController actor, IPhysicsController physicsController, GameWorld world, float dt);
    }
}