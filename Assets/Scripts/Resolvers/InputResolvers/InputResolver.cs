﻿using UnityEngine;

namespace Muffin
{
    public class InputResolver : IInputResolver
    {

        #region Constructors

        public InputResolver () { }

        #endregion // Constructors



        #region Virtual methods

        public virtual void OnJumpDown (ActorController actor, IPhysicsController physicsController, GameWorld world, float dt)
        {
            if (physicsController.JumpCount < physicsController.JumpLimit || physicsController.IsBypassJumpLimit)
            {
                physicsController.IsJumping = true;
                physicsController.JumpCount++;
                float gravity = world.Gravity.y * physicsController.GravityModifier.y;
                float vy = Mathf.Sqrt(-2 * gravity * physicsController.JumpHeight);
                physicsController.SetVelocityY(vy);
            }
        }

        #endregion

    }
}
