﻿using UnityEngine;

namespace Muffin
{
    public interface IPhysicsResolver
    {
        void OnInitialCollisionLeft (ActorController actor, IPhysicsController physicsController, IInputController inputController, RaycastHit2D hit, float dt);
        void OnContinuiousCollisionLeft (ActorController actor, IPhysicsController physicsController, IInputController inputController, RaycastHit2D hit, float dt);

        void OnInitialCollisionRight (ActorController actor, IPhysicsController physicsController, IInputController inputController, RaycastHit2D hit, float dt);
        void OnContinuiousCollisionRight (ActorController actor, IPhysicsController physicsController, IInputController inputController, RaycastHit2D hit, float dt);

        void OnInitialCollisionDown (ActorController actor, IPhysicsController physicsController, IInputController inputController, RaycastHit2D hit, float dt);
        void OnContinuiousCollisionDown (ActorController actor, IPhysicsController physicsController, IInputController inputController, RaycastHit2D hit, float dt);

        void OnInitialCollisionUp (ActorController actor, IPhysicsController physicsController, IInputController inputController, RaycastHit2D hit, float dt);
        void OnContinuiousCollisionUp (ActorController actor, IPhysicsController physicsController, IInputController inputController, RaycastHit2D hit, float dt);
    }
}