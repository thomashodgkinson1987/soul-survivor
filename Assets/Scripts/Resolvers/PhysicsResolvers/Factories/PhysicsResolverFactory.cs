﻿namespace Muffin
{
    public class PhysicsResolverFactory : IPhysicsResolverFactory
    {

        #region Constructors

        public PhysicsResolverFactory () { }

        #endregion // Constructors



        #region IPhysicsResolverFactory methods

        public IPhysicsResolver CreatePhysicsResolver (int id = 0)
        {
            switch (id)
            {
                case 0:
                    return new PhysicsResolver();

                default:
                    return new PhysicsResolver();
            }
        }

        #endregion // IPhysicsResolverFactory methods

    }
}