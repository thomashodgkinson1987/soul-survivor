﻿namespace Muffin
{
    public interface IPhysicsResolverFactory
    {
        IPhysicsResolver CreatePhysicsResolver (int id = 0);
    }
}
