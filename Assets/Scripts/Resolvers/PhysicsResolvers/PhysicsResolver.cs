﻿using UnityEngine;

namespace Muffin
{
    public class PhysicsResolver : IPhysicsResolver
    {

        #region Constructors

        public PhysicsResolver () { }

        #endregion // Constructors



        #region Virtual methods

        public virtual void OnInitialCollisionLeft (ActorController actor, IPhysicsController physicsController, IInputController inputController, RaycastHit2D hit, float dt)
        {
        }
        public virtual void OnContinuiousCollisionLeft (ActorController actor, IPhysicsController physicsController, IInputController inputController, RaycastHit2D hit, float dt)
        {
            actor.SetPositionX(hit.point.x - actor.Collider.offset.x + actor.Bounds.extents.x);
            physicsController.SetVelocityX(0f);
        }

        public virtual void OnInitialCollisionRight (ActorController actor, IPhysicsController physicsController, IInputController inputController, RaycastHit2D hit, float dt)
        {
        }
        public virtual void OnContinuiousCollisionRight (ActorController actor, IPhysicsController physicsController, IInputController inputController, RaycastHit2D hit, float dt)
        {
            actor.SetPositionX(hit.point.x - actor.Collider.offset.x - actor.Bounds.extents.x);
            physicsController.SetVelocityX(0f);
        }

        public virtual void OnInitialCollisionDown (ActorController actor, IPhysicsController physicsController, IInputController inputController, RaycastHit2D hit, float dt)
        {
            physicsController.JumpCount = 0;
        }
        public virtual void OnContinuiousCollisionDown (ActorController actor, IPhysicsController physicsController, IInputController inputController, RaycastHit2D hit, float dt)
        {
            actor.SetPositionY(hit.point.y - actor.Collider.offset.y + actor.Bounds.extents.y);
            physicsController.SetVelocityY(0f);

            if (hit.collider.GetComponent<FallingPlatformController>() != null)
            {
                hit.collider.GetComponent<FallingPlatformController>().Fall();
            }
            else if (hit.collider.GetComponentInParent<MovingPlatformController>() != null)
            {
                MovingPlatformController platform = hit.collider.GetComponentInParent<MovingPlatformController>();

                if (inputController.InputDirection.x != 0)
                {
                    physicsController.TranslateVelocityX((platform.Velocity.x * inputController.InputDirection.x) * dt);
                }
                else
                {
                    physicsController.SetVelocityX(platform.Velocity.x);
                }

                if (platform.Velocity.y < 0)
                {
                    physicsController.SetVelocityY(platform.Velocity.y);
                }
            }
        }

        public virtual void OnInitialCollisionUp (ActorController actor, IPhysicsController physicsController, IInputController inputController, RaycastHit2D hit, float dt)
        {
        }
        public virtual void OnContinuiousCollisionUp (ActorController actor, IPhysicsController physicsController, IInputController inputController, RaycastHit2D hit, float dt)
        {
            actor.SetPositionY(hit.point.y - actor.Collider.offset.y - actor.Bounds.extents.y);
            physicsController.SetVelocityY(0f);
        }

        #endregion // Virtual methods

    }
}
