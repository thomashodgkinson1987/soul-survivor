﻿using CreativeSpore.SuperTilemapEditor;
using UnityEngine;
using Zenject;

namespace Muffin
{
    public class GameController : MonoBehaviour
    {

        #region Editor

        [HideInInspector]
        public bool m_timeFoldout = true;

        #endregion // Editor



        #region Fields

        // TODO: Remove this time settings
        [SerializeField]
        private TimeSettings m_timeSettings;

        [Space]

        [SerializeField]
        private AudioClip m_musicToPlayAtStart;

        [Space]

        [SerializeField]
        private Transform m_parentTransformForActorSpawns;

        [Space]

        [SerializeField]
        private Transform m_levelTransform;
        [SerializeField]
        private TilemapGroup m_tilemapGroup;
        [SerializeField]
        private STETilemap m_mapBounds;

        [Space]

        [SerializeField]
        private CameraController m_cameraController;

        // Zenject construct

        private IAudioManager m_audioManager;
        private IActorControllerFactory m_actorControllerFactory;
        private IRepositoryLoader m_repositoryLoader;
        private GameWorld m_world;

        // Misc
        // TODO: Tidy this up

        private ActorController m_playerActorController;
        private float m_fixedTickTimer = 0f;
        private float m_elapsedTime = 0f;
        private float m_fixedElapsedTime = 0f;
        private bool m_isQuitting = false;

        #endregion // Fields



        #region Properties

        public TimeSettings TimeSettings
        {
            get { return m_timeSettings; }
            set { m_timeSettings = value; }
        }

        #endregion // Properties



        #region Zenject construct

        [Inject]
        private void Construct (
            IAudioManager audioManager,
            IActorControllerFactory actorControllerFactory,
            IRepositoryLoader repositoryLoader,
            GameWorld world)
        {
            m_audioManager = audioManager;
            m_actorControllerFactory = actorControllerFactory;
            m_repositoryLoader = repositoryLoader;
            m_world = world;
        }

        #endregion // Zenject construct



        #region MonoBehaviour methods

        private void Awake ()
        {
            TimeSettings = Instantiate(TimeSettings);
            OnTimeSettingsUpdated();
            m_repositoryLoader.LoadRepositories();
        }

        private void Start ()
        {
            FindAndAddObjectsToGameWorld();
            m_audioManager.PlayMusic(m_musicToPlayAtStart, 0ul, true);

            if (m_mapBounds)
            {
                m_cameraController.SetBounds(m_mapBounds.MapBounds);
            }
        }

        private void Update ()
        {
            // TODO: Add some kind of state machine here
            if (m_isQuitting) return;

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                m_isQuitting = true;
                Quit();
                return;
            }

            float dt = Time.deltaTime;
            float fdt = TimeSettings.FixedDeltaTime;

            m_elapsedTime += dt;

            Tick(dt);

            m_fixedTickTimer += dt;
            while (m_fixedTickTimer >= fdt)
            {
                m_fixedTickTimer -= fdt;
                m_fixedElapsedTime += fdt;
                FixedTick(fdt);
            }

            LateTick(dt);
        }

        #endregion // MonoBehaviour methods



        #region Settings updated methods

        public void OnTimeSettingsUpdated ()
        {
            TimeSettings.FixedDeltaTime = TimeSettings.FixedDeltaTime;
            Time.fixedDeltaTime = TimeSettings.FixedDeltaTime;

            TimeSettings.TimeScale = TimeSettings.TimeScale;
            Time.timeScale = TimeSettings.TimeScale;
        }

        #endregion // Settings updated methods



        #region Tick methods

        private void Tick (float dt)
        {
            m_world.Tick(dt);
        }
        private void FixedTick (float dt)
        {
            m_world.FixedTick(dt);
            UpdateCamera(dt);
        }
        private void LateTick (float dt)
        {
            m_world.LateTick(dt);
        }

        #endregion // Tick methods



        #region Private methods

        private void FindAndAddObjectsToGameWorld ()
        {
            FindAndAddActorSpawnPointsToGameWorld();
            FindAndAddCustomActorSpawnPointsToGameWorld();
            FindAndAddFallingPlatformsToGameWorld();
            FindAndAddMovingPlatformsToGameWorld();
        }
        private void FindAndAddActorSpawnPointsToGameWorld ()
        {
            ActorSpawnPoint[] spawnPoints = FindObjectsOfType<ActorSpawnPoint>();

            if (spawnPoints.Length > 0)
            {
                Transform parentTransform = spawnPoints[0].transform.parent;

                for (int i = 0; i < spawnPoints.Length; i++)
                {
                    ActorSpawnPoint spawnPoint = spawnPoints[i];

                    int id = spawnPoint.ActorId;
                    Vector3 position = spawnPoint.transform.position;
                    ActorController actor = m_actorControllerFactory.CreateActorFromId(id, position);

                    SetSuitableParentTransform(actor);

                    m_world.AddActor(actor);

                    // TODO: Find better way of detecting player
                    if (id == 1 && m_playerActorController == null)
                    {
                        m_playerActorController = actor;
                    }

                    Destroy(spawnPoint.gameObject);
                }

                if (parentTransform != null)
                {
                    Destroy(parentTransform.gameObject);
                }
            }
        }
        private void FindAndAddCustomActorSpawnPointsToGameWorld ()
        {
            CustomActorSpawnPoint[] customSpawnPoints = FindObjectsOfType<CustomActorSpawnPoint>();
            for (int i = 0; i < customSpawnPoints.Length; i++)
            {
                CustomActorSpawnPoint spawnPoint = customSpawnPoints[i];

                ActorController actor = m_actorControllerFactory.CreateCustomActor(
                    spawnPoint.Name,
                    spawnPoint.PluralName,
                    spawnPoint.Width,
                    spawnPoint.Height,
                    spawnPoint.InputControllerId,
                    spawnPoint.InputResolverId,
                    spawnPoint.PhysicsControllerId,
                    spawnPoint.PhysicsResolverId,
                    spawnPoint.PhysicsControllerDataId,
                    spawnPoint.AnimatorControllerId,
                    spawnPoint.transform.position);

                SetSuitableParentTransform(actor);

                m_world.AddActor(actor);

                Destroy(spawnPoint.gameObject);
            }
        }
        private void FindAndAddFallingPlatformsToGameWorld ()
        {
            FallingPlatformController[] fallingPlatformsInScene = FindObjectsOfType<FallingPlatformController>();
            for (int i = 0; i < fallingPlatformsInScene.Length; i++)
            {
                m_world.AddFallingPlatform(fallingPlatformsInScene[i]);
            }
        }
        private void FindAndAddMovingPlatformsToGameWorld ()
        {
            MovingPlatformController[] movingPlatformsInScene = FindObjectsOfType<MovingPlatformController>();
            for (int i = 0; i < movingPlatformsInScene.Length; i++)
            {
                m_world.AddMovingPlatform(movingPlatformsInScene[i]);
            }
        }

        private void UpdateCamera (float dt)
        {
            if (m_playerActorController != null)
            {
                m_cameraController.SetTarget(m_playerActorController.Position);
            }
            m_cameraController.Tick(dt);
        }

        private void SetSuitableParentTransform (ActorController actor)
        {
            ActorComponent actorComponent = actor.GetActorComponent();

            if (m_parentTransformForActorSpawns.Find(actor.Name + "s"))
            {
                actorComponent.transform.SetParent(m_parentTransformForActorSpawns.Find(actor.Name + "s"));
            }
            else
            {
                GameObject newGameObject = new GameObject(actor.Name + "s");
                newGameObject.transform.SetParent(m_parentTransformForActorSpawns);
                actorComponent.transform.SetParent(newGameObject.transform);
            }
        }

        private void Quit ()
        {
            m_isQuitting = true;

            m_audioManager.StopMusic();
            m_audioManager.StopAllSounds();

            m_world.Dispose();
            m_world = null;

            for (int i = 0; i < m_tilemapGroup.Tilemaps.Count; i++)
            {
                Destroy(m_tilemapGroup.Tilemaps[i].gameObject);
            }
            Destroy(m_tilemapGroup.gameObject);

            Destroy(m_levelTransform.gameObject);
            Destroy(m_parentTransformForActorSpawns.gameObject);
            Destroy(gameObject);

            Destroy(m_cameraController.gameObject);

            Application.Quit();
        }

        #endregion // Private methods

    }
}
