﻿using System.Collections.Generic;
using UnityEngine;

namespace Muffin
{
    public class CameraController : MonoBehaviour
    {

        #region Editor

        [HideInInspector]
        public bool m_cameraFoldout = true;

        #endregion // Editor



        #region Fields

        [SerializeField]
        private CameraSettings m_cameraSettings;

        [Space]

        [SerializeField]
        private Camera m_camera;

        [SerializeField]
        private Transform m_target;

        [Space]

        [SerializeField]
        private bool m_isShaking = false;

        private List<CameraShake> m_shakes = new List<CameraShake>();

        #endregion // Fields



        #region Properties

        public CameraSettings CameraSettings
        {
            get { return m_cameraSettings; }
            set { m_cameraSettings = value; }
        }

        private float Lerp
        {
            get { return m_cameraSettings.Lerp; }
            set { m_cameraSettings.Lerp = value; }
        }
        private bool IsClampToBounds
        {
            get { return m_cameraSettings.IsClampToBounds; }
            set { m_cameraSettings.IsClampToBounds = value; }
        }
        private Bounds Bounds
        {
            get { return m_cameraSettings.Bounds; }
            set { m_cameraSettings.Bounds = value; }
        }
        private Vector3 Offset
        {
            get { return m_cameraSettings.Offset; }
            set { m_cameraSettings.Offset = value; }
        }

        #endregion // Properties



        #region MonoBehaviour methods

        private void Awake ()
        {
            m_cameraSettings = Instantiate(m_cameraSettings);

            OnCameraSettingsUpdated();
        }

        #endregion // MonoBehaviour methods



        #region Settings updated methods

        public void OnCameraSettingsUpdated ()
        {
            Lerp = Lerp;
        }

        #endregion // Settings updated methods



        #region Public methods

        public void Tick (float dt)
        {
            HandleCameraShake(dt);
            ClampTargetToBounds();
            MoveCameraTowardsTarget();
        }

        public void Shake (float duration, float strength)
        {
            m_isShaking = true;
            CameraShake shake = new CameraShake(duration, strength);
            m_shakes.Add(shake);
        }

        public void SetBounds (Bounds bounds)
        {
            Bounds = bounds;
        }

        public void SetTarget (float x, float y)
        {
            m_target.position = new Vector3(x, y, m_target.position.z);
        }
        public void SetTarget (Vector3 target)
        {
            SetTarget(target.x, target.y);
        }
        public void SetTarget (List<Bounds> boundsList)
        {
            UpdateBounds(boundsList);
            SetTarget(Bounds.center);
        }

        public void SetPosition (Vector3 position)
        {
            float x = position.x + Offset.x;
            float y = position.y + Offset.y;
            float z = -10f;
            m_camera.transform.position = new Vector3(x, y, z);
        }

        #endregion // Public methods



        #region Private methods

        private void HandleCameraShake (float dt)
        {
            if (m_isShaking)
            {
                for (int i = 0; i < m_shakes.Count; i++)
                {
                    CameraShake shake = m_shakes[i];

                    shake.Timer += dt;

                    float x = Random.Range(-shake.Strength, shake.Strength);
                    float y = Random.Range(-shake.Strength, shake.Strength);
                    Offset = new Vector3(x, y, 0f);

                    if (shake.Timer >= shake.Duration)
                    {
                        m_shakes.Remove(shake);
                        i--;
                    }
                }

                if (m_shakes.Count == 0)
                {
                    m_isShaking = false;
                    Offset = Vector3.zero;
                }
            }
        }

        private void UpdateBounds (List<Bounds> boundsList)
        {
            if (boundsList.Count > 0)
            {
                for (int i = 0; i < boundsList.Count; i++)
                {
                    if (i == 0)
                    {
                        Bounds = new Bounds(boundsList[i].center, boundsList[i].size);
                    }
                    else
                    {
                        Bounds.Encapsulate(boundsList[i]);
                    }
                }
            }
        }
        private void SetTargetToCenterOfBounds ()
        {
            float x = Bounds.center.x;
            float y = Bounds.center.y;
            float z = Bounds.center.z;
            m_target.position = new Vector3(x, y, z);
        }
        private void ClampTargetToBounds ()
        {
            if (IsClampToBounds)
            {
                float height = m_camera.orthographicSize * 2;
                float width = height * m_camera.aspect;

                float x = m_target.position.x;
                float y = m_target.position.y;

                if (width < Bounds.size.x)
                {
                    if (x < Bounds.min.x + (width / 2))
                    {
                        x = Bounds.min.x + (width / 2);
                    }
                    else if (x > Bounds.max.x - (width / 2))
                    {
                        x = Bounds.max.x - (width / 2);
                    }
                }
                else
                {
                    x = Bounds.center.x;
                }

                if (height < Bounds.size.y)
                {
                    if (y < Bounds.min.y + (height / 2))
                    {
                        y = Bounds.min.y + (height / 2);
                    }
                    else if (y > Bounds.max.y - (height / 2))
                    {
                        y = Bounds.max.y - (height / 2);
                    }
                }
                else
                {
                    y = Bounds.center.y;
                }

                SetTarget(x, y);
            }
        }

        private void MoveCameraTowardsTarget ()
        {
            MoveCameraTowardsPoint(m_target.position, Lerp);
        }
        private void MoveCameraToTarget ()
        {
            SetPosition(m_target.position);
        }
        private void MoveCameraTowardsPoint (Vector3 targetPosition, float lerp)
        {
            float x = Mathf.SmoothStep(m_camera.transform.position.x, targetPosition.x, lerp);
            float y = Mathf.SmoothStep(m_camera.transform.position.y, targetPosition.y, lerp);
            float z = Mathf.SmoothStep(m_camera.transform.position.z, targetPosition.z, lerp);
            SetPosition(new Vector3(x, y, z));
        }

        #endregion // Private methods

        private class CameraShake
        {

            #region Fields

            private float m_timer = 0f;
            private float m_duration = 0.2f;
            private float m_strength = 0.1f;

            #endregion // Fields



            #region Properties

            public float Timer
            {
                get { return m_timer; }
                set { m_timer = Mathf.Max(0f, value); }
            }
            public float Duration
            {
                get { return m_duration; }
                set { m_duration = Mathf.Max(0f, value); }
            }
            public float Strength
            {
                get { return m_strength; }
                set { m_strength = Mathf.Max(0f, value); }
            }

            #endregion // Properties



            #region Constructors

            public CameraShake (float duration, float strength)
            {
                Duration = duration;
                Strength = strength;
            }

            #endregion // Constructors

        }

    }
}
