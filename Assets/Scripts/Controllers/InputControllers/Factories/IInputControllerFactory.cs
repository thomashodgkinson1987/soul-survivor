﻿namespace Muffin
{
    public interface IInputControllerFactory
    {
        IInputController CreateInputController (int id = 0);
    }
}