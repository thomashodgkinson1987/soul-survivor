﻿namespace Muffin
{
    public class InputControllerFactory : IInputControllerFactory
    {

        #region Constructors

        public InputControllerFactory () { }

        #endregion // Constructors



        #region Public methods

        public IInputController CreateInputController (int id = 0)
        {
            switch (id)
            {
                case 0:
                    return new DefaultInputController();

                case 1:
                    return new KeyboardInputController();

                case 2:
                    return new JumpingInputController();

                case 3:
                    return new MovingInputController();

                default:
                    return new DefaultInputController();
            }
        }

        #endregion // Public methods

    }
}
