﻿using UnityEngine;

namespace Muffin
{
    public abstract class InputController : IInputController
    {

        #region Fields

        private InputControllerData m_data;

        #endregion // Fields



        #region Properties

        public Vector2 InputDirection
        {
            get
            {
                return new Vector2()
                {
                    x = m_data.InputDirectionX,
                    y = m_data.InputDirectionY
                };
            }
            private set
            {
                m_data.InputDirectionX = value.x;
                m_data.InputDirectionY = value.y;
            }
        }
        public bool IsJumpDown
        {
            get { return m_data.IsJumpDown; }
            private set { m_data.IsJumpDown = value; }
        }

        #endregion // Properties



        #region Constructors

        public InputController ()
        {
            m_data = new InputControllerData();
        }

        #endregion // Constructors



        #region Public methods

        public void Poll ()
        {
            InputDirection = GetInputDirection();
            IsJumpDown = GetIsJumpDown();
        }

        #endregion // Public methods



        #region Abstract methods

        protected abstract Vector2 GetInputDirection ();
        protected abstract bool GetIsJumpDown ();

        #endregion Abstract methods

    }
}
