﻿using UnityEngine;

namespace Muffin
{
    public class KeyboardInputController : InputController
    {

        protected override Vector2 GetInputDirection ()
        {
            return new Vector2()
            {
                x = Input.GetAxisRaw("Horizontal"),
                y = Input.GetAxisRaw("Vertical")
            };
        }

        protected override bool GetIsJumpDown ()
        {
            return Input.GetButtonDown("Jump");
        }

    }
}
