﻿using UnityEngine;

namespace Muffin
{
    public class JumpingInputController : InputController
    {

        #region Constructors

        public JumpingInputController () { }

        #endregion // Constructors



        #region InputController overrides

        protected override Vector2 GetInputDirection ()
        {
            return Vector2.zero;
        }

        protected override bool GetIsJumpDown ()
        {
            return true;
        }

        #endregion // InputController overrides

    }
}
