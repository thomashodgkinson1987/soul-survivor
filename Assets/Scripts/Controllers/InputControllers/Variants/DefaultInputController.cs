﻿using UnityEngine;

namespace Muffin
{
    public class DefaultInputController : InputController
    {

        protected override Vector2 GetInputDirection ()
        {
            return Vector2.zero;
        }

        protected override bool GetIsJumpDown ()
        {
            return false;
        }

    }
}
