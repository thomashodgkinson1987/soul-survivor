﻿using UnityEngine;

namespace Muffin
{
    public class MovingInputController : InputController
    {

        #region Fields

        private float m_timeBetweenJumpsMin = 1f;
        private float m_timeBetweenJumpsMax = 5f;
        private float m_timeBetweenJumps = 1f;
        private float m_jumpTimer = 0f;

        private float m_timeBetweenChangeDirectionMin = 1f;
        private float m_timeBetweenChangeDirectionMax = 3f;
        private float m_timeBetweenChangeDirection = 1f;
        private float m_changeDirectionTimer = 0f;

        private int m_direction = 0;

        #endregion // Fields



        #region Constructors

        public MovingInputController ()
        {
            m_timeBetweenJumps = Random.Range(m_timeBetweenJumpsMin, m_timeBetweenJumpsMax);
            m_jumpTimer = Time.fixedTime + m_timeBetweenJumps;

            m_timeBetweenChangeDirection = Random.Range(m_timeBetweenChangeDirectionMin, m_timeBetweenChangeDirectionMax);
            m_changeDirectionTimer = Time.fixedTime + m_timeBetweenChangeDirection;

            m_direction = Random.Range(-1, 2);
        }

        #endregion // Constructors



        #region InputController overrides

        protected override Vector2 GetInputDirection ()
        {
            if (Time.time >= m_changeDirectionTimer)
            {
                m_timeBetweenChangeDirection = Random.Range(m_timeBetweenChangeDirectionMin, m_timeBetweenChangeDirectionMax);
                m_changeDirectionTimer = Time.fixedTime + m_timeBetweenChangeDirection;

                if (m_direction == -1)
                {
                    m_direction = Random.Range(0, 2);
                }
                else if (m_direction == 0)
                {
                    m_direction = Random.Range(-1, 2);
                }
                else if (m_direction == 1)
                {
                    m_direction = Random.Range(-1, 1);
                }
            }

            return new Vector2(m_direction, 0f);
        }

        protected override bool GetIsJumpDown ()
        {
            if (Time.fixedTime >= m_jumpTimer)
            {
                m_timeBetweenJumps = Random.Range(m_timeBetweenJumpsMin, m_timeBetweenJumpsMax);
                m_jumpTimer = Time.fixedTime + m_timeBetweenJumps;
                return true;
            }

            return false;
        }

        #endregion // InputController overrides

    }
}
