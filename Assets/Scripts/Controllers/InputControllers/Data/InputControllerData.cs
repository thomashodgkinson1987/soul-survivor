﻿namespace Muffin
{
    public class InputControllerData
    {

        #region Properties

        public float InputDirectionX { get; set; }
        public float InputDirectionY { get; set; }
        public bool IsJumpDown { get; set; }

        #endregion // Properties



        #region Constructors

        public InputControllerData (
            float inputDirectionX,
            float inputDirectionY,
            bool isJumpDown)
        {
            InputDirectionX = inputDirectionX;
            InputDirectionY = inputDirectionY;
            IsJumpDown = isJumpDown;
        }
        public InputControllerData () { }

        #endregion // Constructors

    }
}
