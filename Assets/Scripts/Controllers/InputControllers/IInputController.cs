﻿using UnityEngine;

namespace Muffin
{
    public interface IInputController
    {
        Vector2 InputDirection { get; }
        bool IsJumpDown { get; }

        void Poll ();
    }
}