﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Muffin
{
    public class PhysicsController : IPhysicsController
    {

        #region Fields

        private PhysicsControllerData m_data;

        private List<RaycastHit2D> m_hits;

        #endregion // Fields



        #region Properties

        public Vector2 Velocity
        {
            get
            {
                return new Vector2()
                {
                    x = m_data.VelocityX,
                    y = m_data.VelocityY
                };
            }
            private set
            {
                m_data.VelocityX = value.x;
                m_data.VelocityY = value.y;
            }
        }
        public Vector2 MaxVelocity
        {
            get
            {
                return new Vector2()
                {
                    x = m_data.MaxVelocityX,
                    y = m_data.MaxVelocityY
                };
            }
            private set
            {
                m_data.MaxVelocityX = value.x;
                m_data.MaxVelocityY = value.y;
            }
        }

        public Vector2 GravityModifier
        {
            get
            {
                return new Vector2()
                {
                    x = m_data.GravityModifierX,
                    y = m_data.GravityModifierY
                };
            }
            private set
            {
                m_data.GravityModifierX = value.x;
                m_data.GravityModifierY = value.y;
            }
        }

        public float Acceleration
        {
            get { return m_data.Acceleration; }
            private set { m_data.Acceleration = value; }
        }
        public float Deceleration
        {
            get { return m_data.Deceleration; }
            private set { m_data.Deceleration = value; }
        }

        public int JumpCount
        {
            get { return m_data.JumpCount; }
            set { m_data.JumpCount = value; }
        }
        public int JumpLimit
        {
            get { return m_data.JumpLimit; }
            private set { m_data.JumpLimit = value; }
        }
        public float JumpHeight
        {
            get { return m_data.JumpHeight; }
        }
        public bool IsJumpQueued
        {
            get { return m_data.IsJumpQueued; }
            set { m_data.IsJumpQueued = value; }
        }

        public bool IsApplyGravity
        {
            get { return m_data.IsApplyGravity; }
            private set { m_data.IsApplyGravity = value; }
        }
        public bool IsClampVelocity
        {
            get { return m_data.IsApplyGravity; }
            private set { m_data.IsApplyGravity = value; }
        }
        public bool IsBypassJumpLimit
        {
            get { return m_data.IsBypassJumpLimit; }
            private set { m_data.IsBypassJumpLimit = value; }
        }

        public float Margin
        {
            get { return m_data.Margin; }
            private set { m_data.Margin = value; }
        }

        public int HorizontalRayCount
        {
            get { return m_data.HorizontalRayCount; }
            private set { m_data.HorizontalRayCount = value; }
        }
        public int VerticalRayCount
        {
            get { return m_data.VerticalRayCount; }
            private set { m_data.VerticalRayCount = value; }
        }

        public LayerMask LeftLayerMask // TODO: Optimise this? Setter doesn't seem right...
        {
            get
            {
                int finalMask = 0;
                string[] layerNames = m_data.LeftLayerMask.Split(',');

                if (layerNames.Length > 0)
                {
                    int[] layerMasks = new int[layerNames.Length];

                    for (int i = 0; i < layerNames.Length; i++)
                    {
                        layerMasks[i] = 1 << LayerMask.NameToLayer(layerNames[i]);
                        finalMask = finalMask | layerMasks[i];
                    }
                }

                return finalMask;
            }
            private set { m_data.LeftLayerMask = LayerMask.LayerToName(value); }
        }
        public LayerMask RightLayerMask
        {
            get
            {
                int finalMask = 0;
                string[] layerNames = m_data.RightLayerMask.Split(',');

                if (layerNames.Length > 0)
                {
                    int[] layerMasks = new int[layerNames.Length];

                    for (int i = 0; i < layerNames.Length; i++)
                    {
                        layerMasks[i] = 1 << LayerMask.NameToLayer(layerNames[i]);
                        finalMask = finalMask | layerMasks[i];
                    }
                }

                return finalMask;
            }
            private set { m_data.RightLayerMask = LayerMask.LayerToName(value); }
        }
        public LayerMask DownLayerMask
        {
            get
            {
                int finalMask = 0;
                string[] layerNames = m_data.DownLayerMask.Split(',');

                if (layerNames.Length > 0)
                {
                    int[] layerMasks = new int[layerNames.Length];

                    for (int i = 0; i < layerNames.Length; i++)
                    {
                        layerMasks[i] = 1 << LayerMask.NameToLayer(layerNames[i]);
                        finalMask = finalMask | layerMasks[i];
                    }
                }

                return finalMask;
            }
            private set { m_data.DownLayerMask = LayerMask.LayerToName(value); }
        }
        public LayerMask UpLayerMask
        {
            get
            {
                int finalMask = 0;
                string[] layerNames = m_data.UpLayerMask.Split(',');

                if (layerNames.Length > 0)
                {
                    int[] layerMasks = new int[layerNames.Length];

                    for (int i = 0; i < layerNames.Length; i++)
                    {
                        layerMasks[i] = 1 << LayerMask.NameToLayer(layerNames[i]);
                        finalMask = finalMask | layerMasks[i];
                    }
                }

                return finalMask;
            }
            private set { m_data.UpLayerMask = LayerMask.LayerToName(value); }
        }

        public bool IsTouchingLeft
        {
            get { return m_data.IsTouchingLeft; }
            private set { m_data.IsTouchingLeft = value; }
        }
        public bool IsTouchingRight
        {
            get { return m_data.IsTouchingRight; }
            private set { m_data.IsTouchingRight = value; }
        }
        public bool IsTouchingDown
        {
            get { return m_data.IsTouchingDown; }
            private set { m_data.IsTouchingDown = value; }
        }
        public bool IsTouchingUp
        {
            get { return m_data.IsTouchingUp; }
            private set { m_data.IsTouchingUp = value; }
        }

        public bool WasTouchingLeft
        {
            get { return m_data.WasTouchingLeft; }
            private set { m_data.WasTouchingLeft = value; }
        }
        public bool WasTouchingRight
        {
            get { return m_data.WasTouchingRight; }
            private set { m_data.WasTouchingRight = value; }
        }
        public bool WasTouchingDown
        {
            get { return m_data.WasTouchingDown; }
            private set { m_data.WasTouchingDown = value; }
        }
        public bool WasTouchingUp
        {
            get { return m_data.WasTouchingUp; }
            private set { m_data.WasTouchingUp = value; }
        }

        public Vector2 MoveDirection
        {
            get
            {
                return new Vector2()
                {
                    x = Velocity.x == 0 ? 0 : Velocity.x < 0 ? -1 : 1,
                    y = Velocity.y == 0 ? 0 : Velocity.y < 0 ? -1 : 1
                };
            }
        }
        public int FacingDirection
        {
            get { return m_data.FacingDirection; }
            private set { m_data.FacingDirection = value; }
        }
        public bool IsJumping
        {
            get { return m_data.IsJumping; }
            set { m_data.IsJumping = value; }
        }
        public bool IsFalling
        {
            get { return m_data.IsFalling; }
            private set { m_data.IsFalling = value; }
        }

        #endregion // Properties



        #region Constructors

        public PhysicsController (PhysicsControllerData data)
        {
            m_data = data;
            m_hits = new List<RaycastHit2D>();
        }
        public PhysicsController () : this(new PhysicsControllerData()) { }

        #endregion // Constructors



        #region Public methods

        public void SetData (PhysicsControllerData data)
        {
            if (data != null)
            {
                m_data = data;
            }
        }

        public void Tick (ActorController actor, IPhysicsResolver physicsResolver, IInputController inputController, float dt)
        {
            ResetCollisionData();
            ApplyHalfGravity(dt);
            CheckCollisions(actor, physicsResolver, inputController, dt);
            Move(actor, dt);
            if (!IsTouchingDown) ApplyHalfGravity(dt);
            HandleState();
        }

        #endregion // Public methods



        #region Private methods

        private void ResetCollisionData ()
        {
            m_hits.Clear();

            WasTouchingLeft = IsTouchingLeft;
            WasTouchingRight = IsTouchingRight;
            WasTouchingDown = IsTouchingDown;
            WasTouchingUp = IsTouchingUp;

            IsTouchingLeft = false;
            IsTouchingRight = false;
            IsTouchingDown = false;
            IsTouchingUp = false;
        }

        private void ApplyHalfGravity (float dt)
        {
            if (IsApplyGravity)
            {
                TranslateVelocityY(((Physics2D.gravity.y * GravityModifier.y) / 2f) * dt);
            }
        }

        private void CheckCollisions (ActorController actor, IPhysicsResolver physicsResolver, IInputController inputController, float dt)
        {
            if (Velocity.x < 0)
            {
                CheckHorizontal(actor, physicsResolver, inputController, Vector2.left, dt);
            }
            else if (Velocity.x > 0)
            {
                CheckHorizontal(actor, physicsResolver, inputController, Vector2.right, dt);
            }
            else
            {
                CheckHorizontal(actor, physicsResolver, inputController, Vector2.left, dt);
                CheckHorizontal(actor, physicsResolver, inputController, Vector2.right, dt);
            }

            if (Velocity.y < 0)
            {
                CheckVertical(actor, physicsResolver, inputController, Vector2.down, dt);
            }
            else if (Velocity.y > 0)
            {
                CheckVertical(actor, physicsResolver, inputController, Vector2.up, dt);
            }
            else
            {
                CheckVertical(actor, physicsResolver, inputController, Vector2.down, dt);
                CheckVertical(actor, physicsResolver, inputController, Vector2.up, dt);
            }
        }
        private void CheckHorizontal (ActorController actor, IPhysicsResolver physicsResolver, IInputController inputController, Vector2 direction, float dt)
        {
            float moveDistanceThisFrame = (Mathf.Abs(Velocity.x) * dt);
            float margin = Margin;

            int rayCount = HorizontalRayCount;
            float distance = actor.Bounds.extents.x + (moveDistanceThisFrame > margin ? moveDistanceThisFrame : margin);
            LayerMask layerMask = direction == Vector2.left ? LeftLayerMask : RightLayerMask;

            Vector2 startPoint = new Vector2(actor.Bounds.center.x, actor.Bounds.min.y + Margin);
            Vector2 endPoint = new Vector2(actor.Bounds.center.x, actor.Bounds.max.y - Margin);

            UpdateHitsList(rayCount, startPoint, endPoint, direction, distance, layerMask);

            if (m_hits.Any(__hit => __hit.collider != null))
            {
                RaycastHit2D hit = GetShortestRay(m_hits);

                if (hit.normal == Vector2.right)
                {
                    IsTouchingLeft = true;
                    if (!WasTouchingLeft)
                    {
                        physicsResolver.OnInitialCollisionLeft(actor, this, inputController, hit, dt);
                    }
                    physicsResolver.OnContinuiousCollisionLeft(actor, this, inputController, hit, dt);
                }
                else if (hit.normal == Vector2.left)
                {
                    IsTouchingRight = true;
                    if (!WasTouchingRight)
                    {
                        physicsResolver.OnInitialCollisionRight(actor, this, inputController, hit, dt);
                    }
                    physicsResolver.OnContinuiousCollisionRight(actor, this, inputController, hit, dt);
                }
            }
        }
        private void CheckVertical (ActorController actor, IPhysicsResolver physicsResolver, IInputController inputController, Vector2 direction, float dt)
        {
            float moveDistanceThisFrame = (Mathf.Abs(Velocity.y) * dt);
            float margin = Margin;

            int rayCount = VerticalRayCount;
            float distance = actor.Bounds.extents.y + (moveDistanceThisFrame > margin ? moveDistanceThisFrame : margin);
            LayerMask layerMask = direction == Vector2.down ? DownLayerMask : UpLayerMask;

            Vector2 startPoint = new Vector2(actor.Bounds.min.x + Margin, actor.Bounds.center.y);
            Vector2 endPoint = new Vector2(actor.Bounds.max.x - Margin, actor.Bounds.center.y);

            UpdateHitsList(rayCount, startPoint, endPoint, direction, distance, layerMask);

            if (m_hits.Any(__hit => __hit.collider != null))
            {
                RaycastHit2D hit = GetShortestRay(m_hits);

                if (hit.normal == Vector2.up)
                {
                    IsTouchingDown = true;
                    if (!WasTouchingDown)
                    {
                        physicsResolver.OnInitialCollisionDown(actor, this, inputController, hit, dt);
                    }
                    physicsResolver.OnContinuiousCollisionDown(actor, this, inputController, hit, dt);
                }
                else if (hit.normal == Vector2.down)
                {
                    IsTouchingUp = true;
                    if (!WasTouchingUp)
                    {
                        physicsResolver.OnInitialCollisionUp(actor, this, inputController, hit, dt);
                    }
                    physicsResolver.OnContinuiousCollisionUp(actor, this, inputController, hit, dt);
                }
            }
        }

        private void Move (ActorController actor, float dt)
        {
            actor.TranslatePosition(Velocity * dt);
        }

        private void UpdateHitsList (int rayCount, Vector2 startPoint, Vector2 endPoint, Vector2 direction, float distance, LayerMask layerMask)
        {
            rayCount = Mathf.Max(0, rayCount);

            m_hits.Clear();

            for (int i = 0; i < rayCount; i++)
            {
                float lerp = (float) i / (rayCount - 1);

                Vector2 origin = Vector2.Lerp(startPoint, endPoint, lerp);
                RaycastHit2D hit = Physics2D.Raycast(origin, direction, distance, layerMask);

                Debug.DrawRay(origin, direction * distance, Color.red);

                m_hits.Add(hit);
            }
        }
        private int GetIndexOfShortestRay (List<RaycastHit2D> hits)
        {
            int index = -1;
            float distanceOfShortestRay = Mathf.Infinity;

            for (int i = 0; i < hits.Count; i++)
            {
                if (hits[i].collider != null && hits[i].distance < distanceOfShortestRay)
                {
                    distanceOfShortestRay = hits[i].distance;
                    index = i;
                }
            }

            return index;
        }
        private RaycastHit2D GetShortestRay (List<RaycastHit2D> hits)
        {
            int index = GetIndexOfShortestRay(hits);
            return hits[index];
        }

        private void HandleState ()
        {
            if (Velocity.x < 0)
            {
                FacingDirection = -1;
            }
            else if (Velocity.x > 0)
            {
                FacingDirection = 1;
            }

            if (Velocity.y > 0)
            {
                IsFalling = false;
            }
            else if (Velocity.y == 0)
            {
                IsJumping = false;
                IsFalling = false;
            }
            else if (Velocity.y < 0)
            {
                IsJumping = false;
                IsFalling = true;
            }
        }

        #endregion // Private methods



        #region Set methods

        public void SetVelocity (float vx, float vy)
        {
            if (IsClampVelocity)
            {
                vx = Mathf.Clamp(vx, -MaxVelocity.x, MaxVelocity.x);
                vy = Mathf.Clamp(vy, -MaxVelocity.y, MaxVelocity.y);
            }

            Velocity = new Vector2(vx, vy);
        }
        public void SetVelocity (Vector2 velocity)
        {
            SetVelocity(velocity.x, velocity.y);
        }
        public void SetVelocityX (float vx)
        {
            SetVelocity(vx, Velocity.y);
        }
        public void SetVelocityY (float vy)
        {
            SetVelocity(Velocity.x, vy);
        }

        public void TranslateVelocity (float dvx, float dvy)
        {
            SetVelocity(Velocity.x + dvx, Velocity.y + dvy);
        }
        public void TranslateVelocity (Vector2 translation)
        {
            TranslateVelocity(translation.x, translation.y);
        }
        public void TranslateVelocityX (float dvx)
        {
            TranslateVelocity(dvx, 0f);
        }
        public void TranslateVelocityY (float dvy)
        {
            TranslateVelocity(0f, dvy);
        }

        #endregion // Set methods

    }
}
