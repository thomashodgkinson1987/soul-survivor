﻿using UnityEngine;

namespace Muffin
{
    public interface IPhysicsController
    {

        #region Properties

        Vector2 Velocity { get; }
        Vector2 MaxVelocity { get; }

        float Acceleration { get; }
        float Deceleration { get; }

        Vector2 GravityModifier { get; }

        bool IsApplyGravity { get; }
        bool IsBypassJumpLimit { get; }
        bool IsClampVelocity { get; }

        int JumpCount { get; set; }
        int JumpLimit { get; }
        float JumpHeight { get; }
        bool IsJumpQueued { get; set; }

        float Margin { get; }

        int HorizontalRayCount { get; }
        int VerticalRayCount { get; }

        Vector2 MoveDirection { get; }
        int FacingDirection { get; }
        bool IsFalling { get; }
        bool IsJumping { get; set; }

        bool IsTouchingDown { get; }
        bool IsTouchingLeft { get; }
        bool IsTouchingRight { get; }
        bool IsTouchingUp { get; }

        bool WasTouchingDown { get; }
        bool WasTouchingLeft { get; }
        bool WasTouchingRight { get; }
        bool WasTouchingUp { get; }

        LayerMask LeftLayerMask { get; }
        LayerMask RightLayerMask { get; }
        LayerMask UpLayerMask { get; }
        LayerMask DownLayerMask { get; }

        #endregion // Properties



        #region Methods

        void SetData (PhysicsControllerData data);

        void Tick (ActorController actor, IPhysicsResolver physicsResolver, IInputController inputController, float dt);

        void SetVelocity (float vx, float vy);
        void SetVelocity (Vector2 velocity);
        void SetVelocityX (float vx);
        void SetVelocityY (float vy);

        void TranslateVelocity (float dvx, float dvy);
        void TranslateVelocity (Vector2 translation);
        void TranslateVelocityX (float dvx);
        void TranslateVelocityY (float dvy);

        #endregion // Methods

    }
}