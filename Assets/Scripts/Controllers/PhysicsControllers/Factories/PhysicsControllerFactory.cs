﻿namespace Muffin
{
    public class PhysicsControllerFactory : IPhysicsControllerFactory
    {

        #region Constructors

        public PhysicsControllerFactory () { }

        #endregion // Constructors



        #region Public methods

        public IPhysicsController CreatePhysicsController (int id = 0)
        {
            switch (id)
            {
                case 0:
                    return new PhysicsController(); // TODO: Fix this

                default:
                    return new PhysicsController();
            }
        }

        #endregion // Public methods

    }
}
