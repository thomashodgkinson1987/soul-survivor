﻿namespace Muffin
{
    public interface IPhysicsControllerFactory
    {
        IPhysicsController CreatePhysicsController (int id = 0);
    }
}