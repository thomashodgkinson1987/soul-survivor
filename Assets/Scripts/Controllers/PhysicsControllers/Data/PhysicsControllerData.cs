﻿namespace Muffin
{
    public class PhysicsControllerData
    {

        #region Properties

        public float VelocityX { get; set; }
        public float VelocityY { get; set; }

        public float MaxVelocityX { get; set; }
        public float MaxVelocityY { get; set; }

        public float GravityModifierX { get; set; }
        public float GravityModifierY { get; set; }

        public float Acceleration { get; set; }
        public float Deceleration { get; set; }

        public int JumpCount { get; set; }
        public int JumpLimit { get; set; }
        public float JumpHeight { get; set; }
        public bool IsJumpQueued { get; set; }

        public bool IsApplyGravity { get; set; }
        public bool IsClampVelocity { get; set; }
        public bool IsBypassJumpLimit { get; set; }

        public float Margin { get; set; }

        public int HorizontalRayCount { get; set; }
        public int VerticalRayCount { get; set; }

        public string LeftLayerMask { get; set; }
        public string RightLayerMask { get; set; }
        public string DownLayerMask { get; set; }
        public string UpLayerMask { get; set; }

        public bool IsTouchingLeft { get; set; }
        public bool IsTouchingRight { get; set; }
        public bool IsTouchingDown { get; set; }
        public bool IsTouchingUp { get; set; }

        public bool WasTouchingLeft { get; set; }
        public bool WasTouchingRight { get; set; }
        public bool WasTouchingDown { get; set; }
        public bool WasTouchingUp { get; set; }

        public int FacingDirection { get; set; }
        public bool IsJumping { get; set; }
        public bool IsFalling { get; set; }

        #endregion // Properties



        #region Constructors

        public PhysicsControllerData () { }

        #endregion // Constructors

    }
}
