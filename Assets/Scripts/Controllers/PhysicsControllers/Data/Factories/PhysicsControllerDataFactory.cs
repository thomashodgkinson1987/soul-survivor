﻿namespace Muffin
{
    public class PhysicsControllerDataFactory : IPhysicsControllerDataFactory
    {

        #region Constructors

        public PhysicsControllerDataFactory () { }

        #endregion // Constructors



        #region Public methods

        public PhysicsControllerData CreateData (
            float maxVelocityX,
            float maxVelocityY,
            float gravityModifierX,
            float gravityModifierY,
            float acceleration,
            float deceleration,
            int jumpLimit,
            float jumpHeight,
            bool isApplyGravity,
            bool isClampVelocity,
            bool isBypassJumpLimit,
            float margin,
            int horizontalRayCount,
            int verticalRayCount,
            string leftLayerMask,
            string rightLayerMask,
            string downLayerMask,
            string upLayerMask)
        {
            PhysicsControllerData data = new PhysicsControllerData()
            {
                VelocityX = 0f,
                VelocityY = 0f,
                MaxVelocityX = maxVelocityX,
                MaxVelocityY = maxVelocityY,
                GravityModifierX = gravityModifierX,
                GravityModifierY = gravityModifierY,
                Acceleration = acceleration,
                Deceleration = deceleration,
                JumpCount = 0,
                JumpLimit = jumpLimit,
                JumpHeight = jumpHeight,
                IsJumpQueued = false,
                IsApplyGravity = isApplyGravity,
                IsClampVelocity = isClampVelocity,
                IsBypassJumpLimit = isBypassJumpLimit,
                Margin = margin,
                HorizontalRayCount = horizontalRayCount,
                VerticalRayCount = verticalRayCount,
                LeftLayerMask = leftLayerMask,
                RightLayerMask = rightLayerMask,
                DownLayerMask = downLayerMask,
                UpLayerMask = upLayerMask,
                IsTouchingLeft = false,
                IsTouchingRight = false,
                IsTouchingDown = false,
                IsTouchingUp = false,
                WasTouchingLeft = false,
                WasTouchingRight = false,
                WasTouchingDown = false,
                WasTouchingUp = false,
                FacingDirection = 0,
                IsJumping = false,
                IsFalling = false
            };

            return data;
        }

        #endregion // Public methods

    }
}
