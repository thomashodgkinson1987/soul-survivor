﻿namespace Muffin
{
    public interface IPhysicsControllerDataFactory
    {
        PhysicsControllerData CreateData (
            float maxVelocityX,
            float maxVelocityY,
            float gravityModifierX,
            float gravityModifierY,
            float acceleration,
            float deceleration,
            int jumpLimit,
            float jumpHeight,
            bool isApplyGravity,
            bool isClampVelocity,
            bool isBypassJumpLimit,
            float margin,
            int horizontalRayCount,
            int verticalRayCount,
            string leftLayerMask,
            string rightLayerMask,
            string downLayerMask,
            string upLayerMask);
    }
}