﻿using UnityEngine;

namespace Muffin
{
    public class ActorController
    {

        #region Fields

        private ActorControllerData m_data;
        private ActorComponent m_component;

        private IInputController m_inputController;
        private IInputResolver m_inputResolver;
        private IPhysicsController m_physicsController;
        private IPhysicsResolver m_physicsResolver;

        #endregion // Fields



        #region Properties

        public string Name
        {
            get { return m_data.Name; }
            private set { m_data.Name = value; }
        }
        public string PluralName
        {
            get { return m_data.PluralName; }
            private set { m_data.PluralName = value; }
        }

        public Vector2 Position
        {
            get
            {
                return new Vector2()
                {
                    x = m_component.transform.position.x,
                    y = m_component.transform.position.y
                };
            }
            private set
            {
                m_component.transform.position = new Vector3()
                {
                    x = value.x,
                    y = value.y,
                    z = 0f
                };
            }
        }

        public float Width
        {
            get { return m_data.Width; }
            private set { m_data.Width = value; }
        }
        public float Height
        {
            get { return m_data.Height; }
            private set { m_data.Height = value; }
        }

        public BoxCollider2D Collider
        {
            get { return m_component.GetBoxCollider2D(); }
        }
        public Bounds Bounds
        {
            get { return m_component.GetBoxCollider2D().bounds; }
        }

        #endregion // Properties



        #region Constructors

        public ActorController (
            ActorControllerData data,
            ActorComponent component,
            IInputController inputController,
            IInputResolver inputResolver,
            IPhysicsController physicsController,
            IPhysicsResolver physicsResolver)
        {
            m_data = data;
            m_component = component;
            m_inputController = inputController;
            m_inputResolver = inputResolver;
            m_physicsController = physicsController;
            m_physicsResolver = physicsResolver;
        }

        #endregion // Constructors



        #region Tick methods

        public void Tick (GameWorld world, float dt)
        {
            m_inputController.Poll();

            if (m_inputController.IsJumpDown)
            {
                m_physicsController.IsJumpQueued = true;
            }
        }
        public void FixedTick (GameWorld world, float dt)
        {
            RespondToInput(world, dt);
            m_physicsController.Tick(this, m_physicsResolver, m_inputController, dt);
        }
        public void LateTick (GameWorld world, float dt)
        {
            HandleAnimation();
        }

        #endregion // Tick methods



        #region Public methods

        public ActorComponent GetActorComponent ()
        {
            return m_component;
        }

        public void SetData (ActorControllerData data)
        {
            if (data != null)
            {
                m_data = data;
            }
        }
        public void SetComponent (ActorComponent component)
        {
            if (component != null)
            {
                m_component = component;
            }
        }

        public void SetAnimatorController (RuntimeAnimatorController controller)
        {
            if (controller != null)
            {
                m_component.GetAnimator().runtimeAnimatorController = controller;
            }
        }
        public void SetAnimatorController (AnimatorOverrideController controller)
        {
            if (controller != null)
            {
                m_component.GetAnimator().runtimeAnimatorController = controller;
            }
        }

        public void SetComponentsFromBrain (IBrain brain)
        {
            if (brain != null)
            {
                SetInputController(brain.GetInputController());
                SetInputResolver(brain.GetInputResolver());
                SetPhysicsController(brain.GetPhysicsController());
                SetPhysicsResolver(brain.GetPhysicsResolver());
            }
        }
        public void SetInputController (IInputController inputController)
        {
            if (inputController != null)
            {
                m_inputController = inputController;
            }
        }
        public void SetInputResolver (IInputResolver inputResolver)
        {
            if (inputResolver != null)
            {
                m_inputResolver = inputResolver;
            }
        }
        public void SetPhysicsController (IPhysicsController physicsController)
        {
            if (physicsController != null)
            {
                m_physicsController = physicsController;
            }
        }
        public void SetPhysicsResolver (IPhysicsResolver physicsResolver)
        {
            if (physicsResolver != null)
            {
                m_physicsResolver = physicsResolver;
            }
        }

        public void SetName (string name)
        {
            Name = name;
            m_component.SetName(Name);
        }
        public void SetPluralName (string pluralName)
        {
            PluralName = pluralName;
        }

        public void SetPosition (float x, float y)
        {
            Position = new Vector2(x, y);
        }
        public void SetPosition (Vector2 position)
        {
            SetPosition(position.x, position.y);
        }
        public void SetPositionX (float x)
        {
            SetPosition(x, Position.y);
        }
        public void SetPositionY (float y)
        {
            SetPosition(Position.x, y);
        }

        public void TranslatePosition (float dx, float dy)
        {
            SetPosition(Position.x + dx, Position.y + dy);
        }
        public void TranslatePosition (Vector2 translation)
        {
            TranslatePosition(translation.x, translation.y);
        }
        public void TranslatePositionX (float dx)
        {
            TranslatePosition(dx, 0f);
        }
        public void TranslatePositionY (float dy)
        {
            TranslatePosition(0f, dy);
        }

        public void OnDestroy ()
        {
            m_component.Destroy();
        }

        #endregion // Public methods



        #region Private methods

        private void RespondToInput (GameWorld world, float dt)
        {
            if (m_inputController.InputDirection.x != 0)
            {
                float acceleration =
                    m_physicsController.Velocity.x == 0
                    ? m_physicsController.Acceleration
                    : m_inputController.InputDirection.x == m_physicsController.MoveDirection.x
                    ? m_physicsController.Acceleration
                    : m_physicsController.Deceleration;
                m_physicsController.TranslateVelocityX((acceleration * m_inputController.InputDirection.x) * dt);
            }
            else if (m_physicsController.Velocity.x != 0)
            {
                int oldMoveDirection = (int)m_physicsController.MoveDirection.x;
                m_physicsController.TranslateVelocityX((m_physicsController.Deceleration * -oldMoveDirection) * dt);
                if (oldMoveDirection != m_physicsController.MoveDirection.x)
                {
                    m_physicsController.SetVelocityX(0f);
                }
            }

            if (m_physicsController.IsJumpQueued)
            {
                m_physicsController.IsJumpQueued = false;
                m_inputResolver.OnJumpDown(this, m_physicsController, world, dt);
            }
        }
        private void HandleAnimation ()
        {
            m_component.GetSpriteRenderer().flipX = m_physicsController.FacingDirection == -1 ? true : false;

            m_component.GetAnimator().SetBool("IsOnGround", m_physicsController.IsTouchingDown);
            m_component.GetAnimator().SetBool("IsJumping", m_physicsController.IsJumping);
            m_component.GetAnimator().SetBool("IsFalling", m_physicsController.IsFalling);
            m_component.GetAnimator().SetInteger("MoveDirectionX", (int)m_physicsController.MoveDirection.x);
        }

        #endregion // Private methods

    }
}
