﻿using UnityEngine;

namespace Muffin
{
    public class ActorControllerFactory : IActorControllerFactory
    {

        #region Fields

        private readonly IActorControllerDataFactory m_actorControllerDataFactory;

        private readonly IInputControllerFactory m_inputControllerFactory;
        private readonly IInputResolverFactory m_inputResolverFactory;
        private readonly IPhysicsControllerFactory m_physicsControllerFactory;
        private readonly IPhysicsResolverFactory m_physicsResolverFactory;

        private readonly IRepository<ActorControllerData> m_actorControllerDataRepository;
        private readonly IRepository<PhysicsControllerData> m_physicsControllerDataRepository;
        private readonly IRepository<AnimatorOverrideController> m_animatorControllerRepository;

        #endregion // Fields



        #region Constructors

        public ActorControllerFactory (
            IActorControllerDataFactory actorControllerDataFactory,
            IInputControllerFactory inputControllerFactory,
            IInputResolverFactory inputResolverFactory,
            IPhysicsControllerFactory physicsControllerFactory,
            IPhysicsResolverFactory physicsResolverFactory,
            IRepository<ActorControllerData> actorControllerDataRepository,
            IRepository<PhysicsControllerData> physicsControllerDataRepository,
            IRepository<AnimatorOverrideController> animatorControllerRepository)
        {
            m_actorControllerDataFactory = actorControllerDataFactory;
            m_inputControllerFactory = inputControllerFactory;
            m_inputResolverFactory = inputResolverFactory;
            m_physicsControllerFactory = physicsControllerFactory;
            m_physicsResolverFactory = physicsResolverFactory;
            m_actorControllerDataRepository = actorControllerDataRepository;
            m_physicsControllerDataRepository = physicsControllerDataRepository;
            m_animatorControllerRepository = animatorControllerRepository;
        }

        #endregion // Constructors



        #region Public methods

        public ActorController CreateActorFromData (ActorControllerData data, Vector2 position = new Vector2())
        {
            if (data != null)
            {
                GameObject actorGameObject = new GameObject();
                ActorComponent actorComponent = actorGameObject.AddComponent<ActorComponent>();

                IInputController inputController = m_inputControllerFactory.CreateInputController(data.InputControllerId);
                IInputResolver inputResolver = m_inputResolverFactory.CreateInputResolver(data.InputResolverId);
                IPhysicsController physicsController = m_physicsControllerFactory.CreatePhysicsController(data.PhysicsControllerId);
                IPhysicsResolver physicsResolver = m_physicsResolverFactory.CreatePhysicsResolver(data.PhysicsResolverId);

                PhysicsControllerData physicsControllerData = m_physicsControllerDataRepository.GetEntry(data.PhysicsControllerDataId);
                physicsController.SetData(physicsControllerData);

                ActorController controller = new ActorController(
                    data,
                    actorComponent,
                    inputController,
                    inputResolver,
                    physicsController,
                    physicsResolver);

                controller.SetName(data.Name);
                controller.SetPluralName(data.PluralName);
                controller.SetPosition(position);

                AnimatorOverrideController animatorController = m_animatorControllerRepository.GetEntry(data.AnimatorControllerId);
                controller.SetAnimatorController(animatorController);

                return controller;
            }

            return null;
        }

        public ActorController CreateActorFromId (int id = 0, Vector2 position = new Vector2())
        {
            ActorControllerData data = m_actorControllerDataRepository.GetEntry(id);

            return CreateActorFromData(data, position);
        }

        public ActorController CreateCustomActor (
            string name,
            string pluralName,
            float width,
            float height,
            int inputControllerId,
            int inputResolverId,
            int physicsControllerId,
            int physicsResolverId,
            int physicsControllerDataId,
            int animatorControllerId,
            Vector2 position = new Vector2())
        {
            ActorControllerData data = m_actorControllerDataFactory.CreateData(
                name,
                pluralName,
                position.x,
                position.y,
                width,
                height,
                inputControllerId,
                inputResolverId,
                physicsControllerId,
                physicsResolverId,
                physicsControllerDataId,
                animatorControllerId);

            return CreateActorFromData(data);
        }

        public ActorController CreateRandomActor (Vector2 position = new Vector2())
        {
            return CreateActorFromId(Random.Range(0, m_actorControllerDataRepository.Count - 1), position);
        }

        #endregion // Public methods

    }
}
