﻿using UnityEngine;

namespace Muffin
{
    public interface IActorControllerFactory
    {
        ActorController CreateActorFromData (ActorControllerData data, Vector2 position = new Vector2());
        ActorController CreateActorFromId (int id = 0, Vector2 position = new Vector2());
        ActorController CreateCustomActor (
            string name,
            string pluralName,
            float width,
            float height,
            int inputControllerId,
            int inputResolverId,
            int physicsControllerId,
            int physicsResolverId,
            int physicsControllerDataId,
            int animatorControllerId,
            Vector2 position = new Vector2());
        ActorController CreateRandomActor (Vector2 position = new Vector2());
    }
}
