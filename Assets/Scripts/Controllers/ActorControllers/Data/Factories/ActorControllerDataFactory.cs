﻿namespace Muffin
{
    public class ActorControllerDataFactory : IActorControllerDataFactory
    {

        #region Constructors

        public ActorControllerDataFactory () { }

        #endregion // Constructors



        #region Public methods

        public ActorControllerData CreateData (
            string name,
            string pluralName,
            float positionX,
            float positionY,
            float width,
            float height,
            int inputControllerId,
            int inputResolverId,
            int physicsControllerId,
            int physicsResolverId,
            int physicsControllerDataId,
            int animatorControllerId)
        {
            return new ActorControllerData()
            {
                Name = name,
                PluralName = pluralName,
                PositionX = positionX,
                PositionY = positionY,
                Width = width,
                Height = height,
                InputControllerId = inputControllerId,
                InputResolverId = inputResolverId,
                PhysicsControllerId = physicsControllerId,
                PhysicsResolverId = physicsResolverId,
                PhysicsControllerDataId = physicsControllerDataId,
                AnimatorControllerId = animatorControllerId
            };
        }

        #endregion // Public methods

    }
}
