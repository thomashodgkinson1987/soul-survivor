﻿namespace Muffin
{
    public interface IActorControllerDataFactory
    {
        ActorControllerData CreateData (
            string name,
            string pluralName,
            float positionX,
            float positionY,
            float width,
            float height,
            int inputControllerId,
            int inputResolverId,
            int physicsControllerId,
            int physicsResolverId,
            int physicsControllerDataId,
            int animatorControllerId);
    }
}