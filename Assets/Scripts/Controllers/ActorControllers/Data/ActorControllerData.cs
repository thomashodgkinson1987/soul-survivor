﻿namespace Muffin
{
    public class ActorControllerData
    {

        #region Properties

        public string Name { get; set; }
        public string PluralName { get; set; }

        public float PositionX { get; set; }
        public float PositionY { get; set; }

        public float Width { get; set; }
        public float Height { get; set; }

        public int InputControllerId { get; set; }
        public int InputResolverId { get; set; }
        public int PhysicsControllerId { get; set; }
        public int PhysicsResolverId { get; set; }

        public int PhysicsControllerDataId { get; set; }

        public int AnimatorControllerId { get; set; }

        #endregion // Properties



        #region Constructors

        public ActorControllerData () { }

        #endregion // Constructors

    }
}
