﻿using UnityEngine;

namespace Muffin
{
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(EdgeCollider2D))]
    public class FallingPlatformController : MonoBehaviour
    {

        #region Enums

        private enum EState { Idle, Contact, PreFall, Falling, Despawn, WaitingForRespawn }

        #endregion // Enums



        #region Fields

        [SerializeField]
        private Vector3 m_velocity = Vector3.zero;

        [Space]

        [SerializeField]
        private Vector2 m_gravityModifier = Vector2.one;

        [Space]

        [SerializeField]
        private float m_timeBeforeFall = 0.25f;

        [SerializeField]
        private float m_timeBeforeDestroy = 5f;

        [SerializeField]
        private float m_timeBeforeRespawn = 1f;

        [Space]

        [SerializeField]
        private float m_timeBeforeFallTimer = 0f;

        [SerializeField]
        private float m_timeBeforeDestroyTimer = 0f;

        [SerializeField]
        private float m_timeBeforeRespawnTimer = 0f;

        [Space]

        [SerializeField]
        private bool m_isActivated = false;

        private Vector3 m_originalPosition = Vector3.zero;

        private EState m_state = EState.Idle;

        private SpriteRenderer m_spriteRenderer;
        private EdgeCollider2D m_edgeCollider2D;

        #endregion // Fields



        #region Events

        private event System.Action OnActivatedEvent;

        #endregion // Events



        #region MonoBehaviour methods

        private void Awake ()
        {
            m_spriteRenderer = GetComponent<SpriteRenderer>();
            m_edgeCollider2D = GetComponent<EdgeCollider2D>();

            m_originalPosition = transform.position;

            OnActivatedEvent += delegate
            {
                m_isActivated = true;
                m_state = EState.Contact;
                OnActivatedEvent = null;
            };
        }

        #endregion // MonoBehaviour methods



        #region Tick methods

        public void Tick (GameWorld world, float dt)
        {
            if (m_isActivated)
            {
                if (m_state == EState.Idle)
                {
                    return;
                }

                if (m_state == EState.Contact)
                {
                    m_timeBeforeFallTimer += dt;

                    if (m_timeBeforeFallTimer >= m_timeBeforeFall)
                    {
                        m_state = EState.PreFall;
                    }
                    else
                    {
                        return;
                    }
                }

                if (m_state == EState.PreFall)
                {
                    m_edgeCollider2D.enabled = false;
                    m_state = EState.Falling;
                }

                if (m_state == EState.Falling)
                {
                    m_timeBeforeDestroyTimer += dt;
                    if (m_timeBeforeDestroyTimer < m_timeBeforeDestroy)
                    {
                        m_velocity.x += ((world.Gravity.x * m_gravityModifier.x) / 2) * dt;
                        m_velocity.y += ((world.Gravity.y * m_gravityModifier.y) / 2) * dt;
                        transform.Translate(m_velocity * dt);
                        m_velocity.x += ((world.Gravity.x * m_gravityModifier.x) / 2) * dt;
                        m_velocity.y += ((world.Gravity.y * m_gravityModifier.y) / 2) * dt;

                        SetAlpha(1f - (m_timeBeforeDestroyTimer / m_timeBeforeDestroy));

                        return;
                    }
                    else
                    {
                        m_state = EState.Despawn;
                    }
                }

                if (m_state == EState.Despawn)
                {
                    Despawn();
                    m_state = EState.WaitingForRespawn;
                }

                if (m_state == EState.WaitingForRespawn)
                {
                    m_timeBeforeRespawnTimer += dt;
                    if (m_timeBeforeRespawnTimer < m_timeBeforeRespawn)
                    {
                        SetAlpha(m_timeBeforeRespawnTimer / m_timeBeforeRespawn);
                    }
                    else
                    {
                        Respawn();
                    }
                }
            }
        }

        #endregion // Tick methods



        #region Public methods

        public void Fall ()
        {
            if (OnActivatedEvent != null)
            {
                OnActivatedEvent();
            }
        }

        #endregion // Public methods



        #region Private methods

        private void SetAlpha (float alpha)
        {
            Color color = m_spriteRenderer.color;
            color.a = alpha;
            m_spriteRenderer.color = color;
        }

        private void Despawn ()
        {
            transform.position = m_originalPosition;
            m_velocity = Vector3.zero;
            SetAlpha(0f);
        }

        private void Respawn ()
        {
            m_edgeCollider2D.enabled = true;

            SetAlpha(1f);

            m_timeBeforeFallTimer = 0f;
            m_timeBeforeDestroyTimer = 0f;
            m_timeBeforeRespawnTimer = 0f;

            m_isActivated = false;

            m_state = EState.Idle;

            OnActivatedEvent += delegate
            {
                m_isActivated = true;
                m_state = EState.Contact;
                OnActivatedEvent = null;
            };
        }

        #endregion // Private methods

    }
}
