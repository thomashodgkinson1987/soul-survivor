﻿using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Muffin
{
    public class MovingPlatformController : MonoBehaviour
    {

        #region Fields

        [SerializeField]
        private Transform m_platformTransform;

        [Space]

        [SerializeField]
        private float m_speed = 1f;

        [SerializeField]
        private Vector3 m_velocity = Vector3.zero;

        [Space]

        [SerializeField]
        private List<Transform> m_nodes = new List<Transform>();

        [SerializeField]
        private int m_currentNodeIndex = 0;

        #endregion // Fields



        #region Properties

        public float Speed
        {
            get { return m_speed; }
            set { m_speed = Mathf.Max(0f, value); }
        }

        public Vector3 Velocity
        {
            get { return m_velocity; }
            set { m_velocity = value; }
        }

        public List<Transform> Nodes
        {
            get { return m_nodes; }
            set { m_nodes = value; }
        }

        public int CurrentNodeIndex
        {
            get { return m_currentNodeIndex; }
            set { m_currentNodeIndex = Mathf.Max(0, value); }
        }

        #endregion // Properties



        #region MonoBehaviour methods

        private void OnValidate ()
        {
            Speed = Speed;
            CurrentNodeIndex = CurrentNodeIndex;
        }

        #endregion // MonoBehaviour methods



        #region Tick methods

        public void Tick (float dt)
        {
            if (Nodes.Count == 0)
            {
                return;
            }
            else if (Nodes.Count == 1)
            {
                CurrentNodeIndex = 0;
                m_platformTransform.position = Nodes[CurrentNodeIndex].position;
            }
            else
            {
                if (CurrentNodeIndex >= 0 && CurrentNodeIndex < Nodes.Count)
                {
                    MoveTowardsNode(Nodes[CurrentNodeIndex], dt);
                }
            }
        }

        #endregion // Tick methods



        #region Private methods

        private void MoveTowardsNode (Transform node, float dt)
        {
            if (Vector3.Distance(m_platformTransform.position, node.position) > Speed * dt)
            {
                Vector3 futurePosition = Vector3.MoveTowards(m_platformTransform.position, node.position, Speed);
                Vector3 direction = (futurePosition - m_platformTransform.position).normalized;
                Velocity = direction * Speed;
            }
            else
            {
                m_platformTransform.position = node.position;
                Velocity = Vector3.zero;
                SetToNextNode();
            }

            m_platformTransform.Translate(Velocity * dt);
        }

        private void SetToNextNode ()
        {
            if (CurrentNodeIndex + 1 < Nodes.Count)
            {
                CurrentNodeIndex++;
            }
            else
            {
                CurrentNodeIndex = 0;
            }
        }

        #endregion // Private methods



#if UNITY_EDITOR

        #region Debug

        private void OnDrawGizmos ()
        {
            Color originalHandlesColor = Handles.color;
            Handles.color = Color.white;

            for (int i = 0; i < Nodes.Count; i++)
            {
                Handles.DrawSolidDisc(Nodes[i].position, Vector3.forward, 0.1f);

                if (i != Nodes.Count - 1)
                {
                    int nextNodeIndex = i + 1 == Nodes.Count ? 0 : i + 1;
                    Handles.DrawDottedLine(Nodes[i].position, Nodes[nextNodeIndex].position, 1f);
                }
            }

            Handles.color = originalHandlesColor;
        }

        #endregion // Debug

#endif

    }
}