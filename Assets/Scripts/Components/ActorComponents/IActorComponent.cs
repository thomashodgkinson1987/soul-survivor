﻿using UnityEngine;

namespace Muffin
{
    public interface IActorComponent
    {
        SpriteRenderer SpriteRenderer { get; }
        Animator Animator { get; }
        BoxCollider2D BoxCollider2D { get; }

        void SetName (string name);
        void SetPosition (Vector2 position);
        void Destroy ();
    }
}