﻿using UnityEngine;

namespace Muffin
{
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(BoxCollider2D))]
    public class ActorComponent : MonoBehaviour
    {

        #region Fields

        private ActorController m_actorController;

        private SpriteRenderer m_spriteRenderer;
        private Animator m_animator;
        private BoxCollider2D m_boxCollider2d;

        #endregion // Fields



        #region MonoBehaviour methods

        private void Awake ()
        {
            m_spriteRenderer = GetComponent<SpriteRenderer>();
            m_animator = GetComponent<Animator>();
            m_boxCollider2d = GetComponent<BoxCollider2D>();
        }

        #endregion // MonoBehaviour methods



        #region Tick methods

        public void Tick (GameWorld world, float dt)
        {
            if (m_actorController != null)
            {
                m_actorController.Tick(world, dt);
            }
        }
        public void FixedTick (GameWorld world, float dt)
        {
            if (m_actorController != null)
            {
                m_actorController.FixedTick(world, dt);
            }
        }
        public void LateTick (GameWorld world, float dt)
        {
            if (m_actorController != null)
            {
                m_actorController.LateTick(world, dt);
            }
        }

        #endregion // Tick methods



        #region Public methods

        public void SetName (string name)
        {
            this.name = name;
        }
        public void SetPosition (Vector2 position)
        {
            transform.position = new Vector3(position.x, position.y, 0f);
        }

        public void SetActorController (ActorController actorController)
        {
            if (actorController != null)
            {
                m_actorController = actorController;
            }
        }

        public ActorController GetActorController ()
        {
            return m_actorController;
        }
        public SpriteRenderer GetSpriteRenderer ()
        {
            return m_spriteRenderer;
        }
        public Animator GetAnimator ()
        {
            return m_animator;
        }
        public BoxCollider2D GetBoxCollider2D ()
        {
            return m_boxCollider2d;
        }

        public void Destroy ()
        {
            Destroy(gameObject);
        }

        #endregion // Public methods

    }
}
