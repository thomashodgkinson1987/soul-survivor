﻿using System.Collections.Generic;
using UnityEngine;

namespace Muffin
{
    public class RepositoryLoader : IRepositoryLoader
    {

        #region Fields

        private readonly IRepository<AnimatorOverrideController> m_animatorControllerRepository;
        private readonly IRepositoryDataGetter<AnimatorOverrideController> m_animatorControllerRepositoryDataGetter;

        private readonly IRepository<PhysicsControllerData> m_physicsControllerDataRepository;
        private readonly IRepositoryDataGetter<PhysicsControllerData> m_physicsControllerDataRepositoryDataGetter;

        private readonly IRepository<BrainData> m_brainDataRepository;
        private readonly IRepositoryDataGetter<BrainData> m_brainDataRepositoryDataGetter;

        private readonly IRepository<ActorControllerData> m_actorControllerDataRepository;
        private readonly IRepositoryDataGetter<ActorControllerData> m_actorControllerDataRepositoryDataGetter;

        #endregion // Fields



        #region Constructors

        public RepositoryLoader (
            IRepository<AnimatorOverrideController> animatorControllerRepository,
            IRepositoryDataGetter<AnimatorOverrideController> animatorControllerRepositoryDataGetter,
            IRepository<PhysicsControllerData> physicsControllerDataRepository,
            IRepositoryDataGetter<PhysicsControllerData> physicsControllerDataRepositoryDataGetter,
            IRepository<BrainData> brainDataRepository,
            IRepositoryDataGetter<BrainData> brainDataRepositoryDataGetter,
            IRepository<ActorControllerData> actorControllerDataRepository,
            IRepositoryDataGetter<ActorControllerData> actorControllerDataRepositoryDataGetter)
        {
            m_animatorControllerRepository = animatorControllerRepository;
            m_animatorControllerRepositoryDataGetter = animatorControllerRepositoryDataGetter;
            m_physicsControllerDataRepository = physicsControllerDataRepository;
            m_physicsControllerDataRepositoryDataGetter = physicsControllerDataRepositoryDataGetter;
            m_brainDataRepository = brainDataRepository;
            m_brainDataRepositoryDataGetter = brainDataRepositoryDataGetter;
            m_actorControllerDataRepository = actorControllerDataRepository;
            m_actorControllerDataRepositoryDataGetter = actorControllerDataRepositoryDataGetter;
        }

        #endregion // Constructors



        #region Public methods

        public void LoadRepositories ()
        {
            PopulateAnimatorControllerDataRepository();
            PopulatePhysicsControllerDataRepository();
            PopulateBrainDataRepository();
            PopulateActorControllerDataRepository();
        }

        #endregion // Public methods



        #region Private methods

        private void PopulateAnimatorControllerDataRepository ()
        {
            List<AnimatorOverrideController> entries = m_animatorControllerRepositoryDataGetter.GetData();
            m_animatorControllerRepository.AddEntries(entries);
        }

        private void PopulatePhysicsControllerDataRepository ()
        {
            List<PhysicsControllerData> entries = m_physicsControllerDataRepositoryDataGetter.GetData();
            m_physicsControllerDataRepository.AddEntries(entries);
        }

        private void PopulateBrainDataRepository ()
        {
            List<BrainData> entries = m_brainDataRepositoryDataGetter.GetData();
            m_brainDataRepository.AddEntries(entries);
        }

        private void PopulateActorControllerDataRepository ()
        {
            List<ActorControllerData> entries = m_actorControllerDataRepositoryDataGetter.GetData();
            m_actorControllerDataRepository.AddEntries(entries);
        }

        #endregion // Private methods

    }
}
