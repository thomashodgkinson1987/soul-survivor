﻿using UnityEngine;

namespace Muffin
{
    public class ActorSpawnPoint : MonoBehaviour
    {

        #region Fields

        [SerializeField]
        private int m_actorId = 1;

        #endregion // Fields



        #region Properties

        public int ActorId
        {
            get { return m_actorId; }
        }

        #endregion // Properties

    }
}
