﻿using UnityEngine;

namespace Muffin
{
    public class CustomActorSpawnPoint : MonoBehaviour
    {

        #region Fields

        [SerializeField]
        private string m_name = "DefaultName";
        [SerializeField]
        private string m_pluralName = "DefaultPluralName";
        [SerializeField]
        private float m_width = 1;
        [SerializeField]
        private float m_height = 1;
        [SerializeField]
        private int m_inputControllerId = 0;
        [SerializeField]
        private int m_inputResolverId = 0;
        [SerializeField]
        private int m_physicsControllerId = 0;
        [SerializeField]
        private int m_physicsResolverId = 0;
        [SerializeField]
        private int m_physicsControllerDataId = 0;
        [SerializeField]
        private int m_animatorControllerId = 0;

        #endregion // Fields



        #region Properties

        public string Name
        {
            get { return m_name; }
        }
        public string PluralName
        {
            get { return m_pluralName; }
        }
        public float Width
        {
            get { return m_width; }
        }
        public float Height
        {
            get { return m_height; }
        }
        public int InputControllerId
        {
            get { return m_inputControllerId; }
        }
        public int InputResolverId
        {
            get { return m_inputResolverId; }
        }
        public int PhysicsControllerId
        {
            get { return m_physicsControllerId; }
        }
        public int PhysicsResolverId
        {
            get { return m_physicsResolverId; }
        }
        public int PhysicsControllerDataId
        {
            get { return m_physicsControllerDataId; }
        }
        public int AnimatorControllerId
        {
            get { return m_animatorControllerId; }
        }

        #endregion // Properties

    }
}
