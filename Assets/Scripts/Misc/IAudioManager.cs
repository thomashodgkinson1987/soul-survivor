﻿using UnityEngine;

namespace Muffin
{
    public interface IAudioManager
    {
        void PlayMusic (AudioClip musicClip, ulong delay = 0ul, bool isLoop = false);
        void PlaySound (AudioClip soundClip, float volumeScale = 1f);
        void StopAllSounds ();
        void StopMusic ();
    }
}