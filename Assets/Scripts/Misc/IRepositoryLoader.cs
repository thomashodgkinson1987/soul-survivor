﻿namespace Muffin
{
    public interface IRepositoryLoader
    {
        void LoadRepositories ();
    }
}