﻿using UnityEngine;

namespace Muffin
{
    public class AudioManager : IAudioManager
    {

        #region Fields

        private AudioSource m_musicAudioSource;
        private AudioSource m_soundAudioSource;

        #endregion // Fields



        #region Constructors

        public AudioManager (AudioSource musicAudioSource, AudioSource soundAudioSource)
        {
            m_musicAudioSource = musicAudioSource;
            m_soundAudioSource = soundAudioSource;
        }

        #endregion // Constructors



        #region Public methods

        public void PlayMusic (AudioClip musicClip, ulong delay = 0ul, bool isLoop = false)
        {
            m_musicAudioSource.Stop();

            if (musicClip != null)
            {
                m_musicAudioSource.clip = musicClip;
                m_musicAudioSource.Play(delay);
                m_musicAudioSource.loop = isLoop;
            }
        }

        public void StopMusic ()
        {
            if (m_musicAudioSource.isPlaying)
            {
                m_musicAudioSource.Stop();
            }
        }

        public void PlaySound (AudioClip soundClip, float volumeScale = 1f)
        {
            if (soundClip != null)
            {
                m_soundAudioSource.PlayOneShot(soundClip, volumeScale);
            }
        }

        public void StopAllSounds ()
        {
            if (m_soundAudioSource.isPlaying)
            {
                m_soundAudioSource.Stop();
            }
        }

        #endregion // Public methods

    }
}
