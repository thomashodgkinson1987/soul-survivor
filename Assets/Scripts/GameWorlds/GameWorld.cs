﻿using System.Collections.Generic;
using UnityEngine;

namespace Muffin
{
    public class GameWorld
    {

        #region Fields

        private List<ActorController> m_actors;

        private List<FallingPlatformController> m_fallingPlatforms;
        private List<MovingPlatformController> m_movingPlatforms;

        private float m_elapsedTime;
        private float m_fixedElapsedTime;

        private Vector2 m_gravity;

        private readonly IActorControllerFactory m_actorControllerFactory;

        #endregion // Fields



        #region Properties

        public List<ActorController> Actors
        {
            get { return m_actors; }
            private set { m_actors = value; }
        }
        public List<FallingPlatformController> FallingPlatforms
        {
            get { return m_fallingPlatforms; }
            private set { m_fallingPlatforms = value; }
        }
        public List<MovingPlatformController> MovingPlatforms
        {
            get { return m_movingPlatforms; }
            private set { m_movingPlatforms = value; }
        }

        public Vector2 Gravity
        {
            get { return m_gravity; }
            private set { m_gravity = value; }
        }

        public float ElapsedTime
        {
            get { return m_elapsedTime; }
            private set { m_elapsedTime = value; }
        }
        public float FixedElapsedTime
        {
            get { return m_fixedElapsedTime; }
            private set { m_fixedElapsedTime = value; }
        }

        #endregion // Properties



        #region Constructors

        public GameWorld (IActorControllerFactory actorFactory)
        {
            m_actorControllerFactory = actorFactory;

            m_actors = new List<ActorController>();
            m_fallingPlatforms = new List<FallingPlatformController>();
            m_movingPlatforms = new List<MovingPlatformController>();

            m_gravity = new Vector2(0f, -9.81f);

            m_elapsedTime = 0f;
            m_fixedElapsedTime = 0f;
        }

        #endregion // Constructors



        #region Tick methods

        public void Tick (float dt)
        {
            ElapsedTime += dt;

            if (Input.GetMouseButtonDown(1))
            {
                Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                position.z = 0f;
                SpawnRandomActor(position);
            }

            TickActors(dt);
        }
        public void FixedTick (float dt)
        {
            FixedElapsedTime += dt;
            TickPlatforms(dt);
            FixedTickActors(dt);
        }
        public void LateTick (float dt)
        {
            LateTickActors(dt);
        }

        #endregion // Tick methods



        #region Public methods

        public void SpawnActor (int id = 0, Vector3 position = new Vector3())
        {
            ActorController actor = m_actorControllerFactory.CreateActorFromId(id, position);
            if (actor != null)
            {
                AddActor(actor);
            }
        }

        public void SpawnRandomActor (Vector3 position)
        {
            ActorController actor = m_actorControllerFactory.CreateRandomActor(position);
            if (actor != null)
            {
                AddActor(actor);
            }
        }

        public void AddActor (ActorController actor)
        {
            if (actor != null && !Actors.Contains(actor))
            {
                Actors.Add(actor);
                OnActorAdded(actor);
            }
        }
        public void AddFallingPlatform (FallingPlatformController platform)
        {
            if (platform != null && !FallingPlatforms.Contains(platform))
            {
                FallingPlatforms.Add(platform);
                OnFallingPlatformAdded(platform);
            }
        }
        public void AddMovingPlatform (MovingPlatformController platform)
        {
            if (platform != null && !MovingPlatforms.Contains(platform))
            {
                MovingPlatforms.Add(platform);
                OnMovingPlatformAdded(platform);
            }
        }

        public void RemoveActor (ActorController actor)
        {
            if (actor != null && Actors.Contains(actor))
            {
                Actors.Remove(actor);
                OnActorRemoved(actor);
                actor.OnDestroy();
            }
        }
        public void RemoveFallingPlatform (FallingPlatformController platform)
        {
            if (platform != null && FallingPlatforms.Contains(platform))
            {
                FallingPlatforms.Remove(platform);
                OnFallingPlatformRemoved(platform);
                Object.Destroy(platform.gameObject);
            }
        }
        public void RemoveMovingPlatform (MovingPlatformController platform)
        {
            if (platform != null && MovingPlatforms.Contains(platform))
            {
                MovingPlatforms.Remove(platform);
                OnMovingPlatformRemoved(platform);
                Object.Destroy(platform.gameObject);
            }
        }

        public void RemoveAllActors ()
        {
            for (int i = 0; i < Actors.Count; i++)
            {
                RemoveActor(Actors[i]);
                i--;
            }
            Actors.Clear();
        }
        public void RemoveAllPlatforms ()
        {
            for (int i = 0; i < FallingPlatforms.Count; i++)
            {
                RemoveFallingPlatform(FallingPlatforms[i]);
                i--;
            }
            FallingPlatforms.Clear();

            for (int i = 0; i < MovingPlatforms.Count; i++)
            {
                RemoveMovingPlatform(MovingPlatforms[i]);
                i--;
            }
            MovingPlatforms.Clear();
        }

        public void Dispose ()
        {
            RemoveAllActors();
            Actors = null;

            RemoveAllPlatforms();
            FallingPlatforms = null;
            MovingPlatforms = null;
        }

        #endregion // Public methods



        #region Virtual methods

        public virtual void OnActorAdded (ActorController actor)
        {

        }
        public virtual void OnActorRemoved (ActorController actor)
        {

        }

        public virtual void OnFallingPlatformAdded (FallingPlatformController platform)
        {

        }
        public virtual void OnFallingPlatformRemoved (FallingPlatformController platform)
        {

        }

        public virtual void OnMovingPlatformAdded (MovingPlatformController platform)
        {

        }
        public virtual void OnMovingPlatformRemoved (MovingPlatformController platform)
        {

        }

        #endregion // Virtual methods



        #region Private methods

        private void TickActors (float dt)
        {
            for (int i = 0; i < Actors.Count; i++)
            {
                Actors[i].Tick(this, dt);
            }
        }
        private void FixedTickActors (float dt)
        {
            for (int i = 0; i < Actors.Count; i++)
            {
                Actors[i].FixedTick(this, dt);
            }
        }
        private void LateTickActors (float dt)
        {
            for (int i = 0; i < Actors.Count; i++)
            {
                Actors[i].LateTick(this, dt);
            }
        }

        private void TickPlatforms (float dt)
        {
            for (int i = 0; i < FallingPlatforms.Count; i++)
            {
                FallingPlatforms[i].Tick(this, dt);
            }

            for (int i = 0; i < MovingPlatforms.Count; i++)
            {
                MovingPlatforms[i].Tick(dt);
            }
        }

        #endregion // Private methods

    }
}
