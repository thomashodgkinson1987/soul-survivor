﻿using UnityEngine;
using UnityEditor;

namespace Muffin
{
    [CustomEditor(typeof(CameraController))]
    public class CameraEditor : Editor
    {

        #region Fields

        private CameraController m_camera;

        private Editor m_cameraSettingsEditor;

        #endregion // Fields



        #region Editor methods

        public override void OnInspectorGUI ()
        {
            base.OnInspectorGUI();
            DrawSettingsEditor(m_camera.CameraSettings, m_camera.OnCameraSettingsUpdated, ref m_camera.m_cameraFoldout, ref m_cameraSettingsEditor);
        }

        private void OnEnable ()
        {
            m_camera = (CameraController)target;
        }

        #endregion // Editor methods




        #region Private methods

        private void DrawSettingsEditor (Object settings, System.Action onSettingsUpdated, ref bool foldout, ref Editor editor)
        {
            if (settings != null)
            {
                foldout = EditorGUILayout.InspectorTitlebar(foldout, settings);

                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    if (foldout)
                    {
                        CreateCachedEditor(settings, null, ref editor);
                        editor.OnInspectorGUI();

                        if (check.changed)
                        {
                            if (onSettingsUpdated != null)
                            {
                                onSettingsUpdated();
                            }
                        }
                    }
                }
            }
        }

        #endregion // Private methods

    }
}
