﻿using UnityEngine;
using UnityEditor;

namespace Muffin
{
    [CustomEditor(typeof(GameController))]
    public class GameEditor : Editor
    {

        #region Fields

        private GameController m_game;

        private Editor m_timeSettingsEditor;

        #endregion // Fields



        #region Editor methods

        public override void OnInspectorGUI ()
        {
            base.OnInspectorGUI();
            DrawSettingsEditor(m_game.TimeSettings, m_game.OnTimeSettingsUpdated, ref m_game.m_timeFoldout, ref m_timeSettingsEditor);
        }

        private void OnEnable ()
        {
            m_game = (GameController)target;
        }

        #endregion // Editor methods




        #region Private methods

        private void DrawSettingsEditor (Object settings, System.Action onSettingsUpdated, ref bool foldout, ref Editor editor)
        {
            if (settings != null)
            {
                foldout = EditorGUILayout.InspectorTitlebar(foldout, settings);

                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    if (foldout)
                    {
                        CreateCachedEditor(settings, null, ref editor);
                        editor.OnInspectorGUI();

                        if (check.changed)
                        {
                            if (onSettingsUpdated != null)
                            {
                                onSettingsUpdated();
                            }
                        }
                    }
                }
            }
        }

        #endregion // Private methods

    }
}
