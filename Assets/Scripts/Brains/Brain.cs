﻿namespace Muffin
{
    public abstract class Brain : IBrain
    {
        public abstract IInputController GetInputController ();
        public abstract IInputResolver GetInputResolver ();
        public abstract IPhysicsController GetPhysicsController ();
        public abstract IPhysicsResolver GetPhysicsResolver ();
    }
}
