﻿using UnityEngine;

namespace Muffin
{
    public class BrainFactory : IBrainFactory
    {

        #region Fields

        private readonly IInputControllerFactory m_inputControllerFactory;
        private readonly IInputResolverFactory m_InputResolverFactory;
        private readonly IPhysicsControllerFactory m_physicsControllerFactory;
        private readonly IPhysicsResolverFactory m_physicsResolverFactory;

        private readonly IRepository<BrainData> m_brainDataRepository;

        #endregion // Fields



        #region Constructors

        public BrainFactory (
            IInputControllerFactory inputControllerFactory,
            IInputResolverFactory inputResolverFactory,
            IPhysicsControllerFactory physicsControllerFactory,
            IPhysicsResolverFactory physicsResolverFactory,
            IRepository<BrainData> brainRepository)
        {
            m_inputControllerFactory = inputControllerFactory;
            m_InputResolverFactory = inputResolverFactory;
            m_physicsControllerFactory = physicsControllerFactory;
            m_physicsResolverFactory = physicsResolverFactory;
            m_brainDataRepository = brainRepository;
        }

        #endregion // Constructors



        #region Public methods

        public IBrain CreateBrainFromId (int id = 0)
        {
            if (id >= 0 && id < m_brainDataRepository.Count)
            {
                BrainData brainData = m_brainDataRepository.GetEntry(id);

                return CreateBrainFromSeperateIds(
                    brainData.InputControllerId,
                    brainData.InputResolverId,
                    brainData.PhysicsControllerId,
                    brainData.PhysicsResolverId);
            }

            return null;
        }

        public IBrain CreateRandomBrain ()
        {
            return CreateBrainFromId(Random.Range(0, m_brainDataRepository.Count - 1));
        }

        public IBrain CreateBrainFromSeperateIds (
            int inputControllerId,
            int inputResolverId,
            int physicsControllerId,
            int physicsResolverId)
        {
            return new CustomBrain(
                m_inputControllerFactory.CreateInputController(inputControllerId),
                m_InputResolverFactory.CreateInputResolver(inputResolverId),
                m_physicsControllerFactory.CreatePhysicsController(physicsControllerId),
                m_physicsResolverFactory.CreatePhysicsResolver(physicsResolverId));
        }

        #endregion // Public methods

    }
}
