﻿namespace Muffin
{
    public interface IBrainFactory
    {
        IBrain CreateBrainFromId (int id = 0);
        IBrain CreateBrainFromSeperateIds (int inputControllerId, int inputResolverId, int physicsControllerId, int physicsResolverId);
        IBrain CreateRandomBrain ();
    }
}