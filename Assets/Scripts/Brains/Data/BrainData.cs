﻿namespace Muffin
{
    public class BrainData
    {

        #region Properties

        public int InputControllerId { get; set; }
        public int InputResolverId { get; set; }
        public int PhysicsControllerId { get; set; }
        public int PhysicsResolverId { get; set; }

        #endregion // Properties



        #region Constructors

        public BrainData (
            int inputControllerId,
            int inputResolverId,
            int physicsControllerId,
            int collisionResolverId)
        {
            InputControllerId = inputControllerId;
            InputResolverId = inputResolverId;
            PhysicsControllerId = physicsControllerId;
            PhysicsResolverId = collisionResolverId;
        }
        public BrainData () { }

        #endregion // Constructors

    }
}
