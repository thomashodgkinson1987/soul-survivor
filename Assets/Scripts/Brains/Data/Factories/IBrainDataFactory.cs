﻿namespace Muffin
{
    public interface IBrainDataFactory
    {
        BrainData CreateData (int inputControllerId, int inputResolverId, int physicsControllerId, int collisionResolverId);
    }
}