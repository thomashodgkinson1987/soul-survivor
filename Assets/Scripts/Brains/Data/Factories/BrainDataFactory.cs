﻿namespace Muffin
{
    public class BrainDataFactory : IBrainDataFactory
    {

        #region Constructors

        public BrainDataFactory () { }

        #endregion // Constructors



        #region Public methods

        public BrainData CreateData (
            int inputControllerId,
            int inputResolverId,
            int physicsControllerId,
            int collisionResolverId)
        {
            return new BrainData(
                inputControllerId,
                inputResolverId,
                physicsControllerId,
                collisionResolverId);
        }

        #endregion // Public methods

    }
}
