﻿namespace Muffin
{
    public interface IBrain
    {
        IInputController GetInputController ();
        IInputResolver GetInputResolver ();
        IPhysicsController GetPhysicsController ();
        IPhysicsResolver GetPhysicsResolver ();
    }
}