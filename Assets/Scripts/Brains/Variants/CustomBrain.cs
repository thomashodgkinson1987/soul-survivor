﻿namespace Muffin
{
    public class CustomBrain : Brain
    {

        #region Fields

        private readonly IInputController m_inputController;
        private readonly IInputResolver m_inputResolver;
        private readonly IPhysicsController m_physicsController;
        private readonly IPhysicsResolver m_physicsResolver;

        #endregion // Fields



        #region Constructors

        public CustomBrain (
            IInputController inputController,
            IInputResolver inputResolver,
            IPhysicsController physicsController,
            IPhysicsResolver physicsResolver)
        {
            m_inputController = inputController;
            m_inputResolver = inputResolver;
            m_physicsController = physicsController;
            m_physicsResolver = physicsResolver;
        }

        #endregion // Constructors



        #region ActorBrain overrides

        public override IInputController GetInputController ()
        {
            return m_inputController;
        }

        public override IInputResolver GetInputResolver ()
        {
            return m_inputResolver;
        }

        public override IPhysicsController GetPhysicsController ()
        {
            return m_physicsController;
        }

        public override IPhysicsResolver GetPhysicsResolver ()
        {
            return m_physicsResolver;
        }

        #endregion // ActorBrain overrides

    }
}
