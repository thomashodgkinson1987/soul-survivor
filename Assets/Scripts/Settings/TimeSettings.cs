﻿using UnityEngine;

namespace Muffin
{
    [CreateAssetMenu(fileName = "Time Settings", menuName = "Muffin/New Time Settings")]
    public class TimeSettings : ScriptableObject
    {

        #region Fields

        [SerializeField]
        private float m_fixedDeltaTime = 1f / 50;

        [SerializeField]
        private float m_timeScale = 1f;

        #endregion // Fields



        #region Properties

        public float FixedDeltaTime
        {
            get { return m_fixedDeltaTime; }
            set { m_fixedDeltaTime = Mathf.Clamp(value, 0.01f, 0.5f); }
        }

        public float TimeScale
        {
            get { return m_timeScale; }
            set { m_timeScale = Mathf.Max(0, value); }
        }

        #endregion // Properties

    }
}
