﻿using UnityEngine;

namespace Muffin
{
    [CreateAssetMenu(fileName = "Camera Settings", menuName = "Muffin/New Camera Settings")]
    public class CameraSettings : ScriptableObject
    {

        #region Fields

        [SerializeField]
        private float m_lerp = 0.2f;

        [SerializeField]
        private bool m_isClampToBounds = true;

        [SerializeField]
        private Bounds m_bounds = new Bounds();

        [SerializeField]
        private Vector3 m_offset = Vector3.zero;

        #endregion // Fields



        #region Properties

        public float Lerp
        {
            get { return m_lerp; }
            set { m_lerp = Mathf.Max(0, value); }
        }

        public bool IsClampToBounds
        {
            get { return m_isClampToBounds; }
            set { m_isClampToBounds = value; }
        }

        public Bounds Bounds
        {
            get { return m_bounds; }
            set { m_bounds = value; }
        }

        public Vector3 Offset
        {
            get { return m_offset; }
            set { m_offset = value; }
        }

        #endregion // Properties

    }
}